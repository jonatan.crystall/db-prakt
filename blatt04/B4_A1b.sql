-- was bildet die View eigentlich ab?
create or replace view riverorlake as 
	select name,river,0 as length
	-- Vereinigung von Seen und ...
	from lake l 
		union
			( -- alle eingetragenen Fluesse und ... Fluss->Fluss
			select name,river,nvl(length,0) 
			from river r 
			where r.river is not null
			) 
		union 
			( -- alle eingetragenen Fluesse, die nicht in Seen fliessen??? und ...
			-- muessten diese Fluesse nicht in river r schon enthalten sein?
			select name,lake,nvl(length,0) 
			from river 
			where lake is not null 

			) 
		union 
			( -- die Menge der Fluesse mit Seen die Null sind???? Fluss->Meer
			select name,river,nvl(length,0) 
			from river 
			where river is null 
			and lake is null
			);

set serveroutput on;
-- Rekursive Funktion zur Berechnung der Gesammtlaenge eines Flusssystems 
create or replace function riverlength2 (riv VARCHAR2)
	return number

is
	--Gesammtlaenge der Flusssystems
   len number := 0;
   
begin
	-- Generalisiert Laenge, damit es keine Null laenge gibt
	select nvl(length,0) into len 
	from riverorlake 
	where name = riv;
	
--dbms_output.put_line(len);
for entry in 


	(-- soalnge es Eintraege in roverorlake gibt => Loop
	select * 
	from riverorlake 
	where river = riv
	)
	
loop
		-- rekursive Summierung der Flusslaenge aus riverorlake
		dbms_output.put_line(entry.name || ' ' || entry.length);
  	  	len:=len+riverlength2(entry.name);
	  	
--dbms_output.put_line(entry.river|| ' '  || entry.lake|| ' ' || entry.length);
end loop;
	
	--dbms_output.put_line('loopende');
	--Gesammtlaenge des Flusssystems
	return len;
end;
/
