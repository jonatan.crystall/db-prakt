SET SERVEROUTPUT ON;

-- Symmetric Distances between airports (=> All possible edges)
CREATE OR REPLACE VIEW sDistances AS
    (SELECT Code1, Code2, Dist FROM Distances)
    UNION
    (SELECT Code2, Code1, Dist FROM Distances);

-- Temp table with distances etc. during calculation
CREATE TABLE nodeestimate (
    Code VARCHAR2(50),
    Distance NUMBER,
    Predecessor VARCHAR2(50),
    Done NUMBER(1,0)
);

CREATE OR REPLACE PROCEDURE Dijkstra (
    Startcode VARCHAR2,
    Endcode VARCHAR2,
	Range NUMBER
)
IS
    currentfromcode VARCHAR2;
    currentdistance NUMBER;
BEGIN
    -- Fill nodeestimate with all nodes from sDistances
    INSERT INTO nodeestimate (Code, Distance, Predecessor, Done)
        SELECT Code1, NULL, NULL, 0 from sDistances;

    -- Update estimate for startnode
    UPDATE nodeestimate SET Distance = 0 where Code = Startcode;

    LOOP
        currentfromcode := NULL;
        -- Select node with shortest estimated distance
        SELECT Code, Distance INTO currentfromcode, currentdistance
            FROM nodeestimate
            WHERE Done = 0
            AND Distance IS NOT NULL
            AND ROWNUM = 1
            ORDER BY Distance;
        
        -- no nodes left
        IF currentfromcode IS NULL OR currentfromcode = Endcode
        THEN
            EXIT;
        END IF;
        
        -- current node done
        UPDATE nodeestimate SET Done = 1 WHERE Code = currentfromcode;
        
        -- Update estimated Distances of neighbours
        UPDATE nodeestimate n, sDistances s
            SET n.Distance = currentdistance + s.Distance,
            n.Predecessor = currentfromcode
            WHERE s.Code2 = n.Code
            AND n.done = 0
            AND s.Code1 = currentfromcode
            AND (currentdistance + s.Distance) < n.Distance;

    END LOOP;

END;
/
