set serveroutput on;
create or replace procedure halfGDP
is 
	halfBIP number:= 0;
	addBIP number:= 0;
	worldPop number:= 0;
	relevantPop number:= 0;
begin
	-- HalfBIP ist die Haelfte des globalen BIP, der erreicht werden muss
	select sum(gdp)/2 into halfBIP 
	from economy;
	
	-- Bestimme die Weltbevoelkerung
	select sum(population) into worldPop
	from country;
for entry in 

	(
	select gdp as gdp, population  
	from economy, country  
	where code = country 
	and gdp is not null 
	order by gdp/population desc
	)
	

LOOP
	-- wenn BIP > als Aufgabenwert halfBIP => korriegiere
	if	addBIP + entry.gdp > halfBIP then
			-- was genau soll diese Rechnung bewirken???
			-- sollte man nciht einfach gdp draufrechnen und ausgeben?
			addBIP := (halfBIP-addBIP)/entry.gdp;
			-- addBIP ist jetzt der Anteil des letzten Landes, der notwendig ist
		    relevantPop := relevantPop + addBIP * entry.population;
			relevantPop := relevantPop / worldPop;
			dbms_output.put_line(relevantPop);
			exit;
	end if;
		addBIP := addBIP + entry.gdp;
		relevantPop := relevantPop + entry.population;
END LOOP;
end;
/
