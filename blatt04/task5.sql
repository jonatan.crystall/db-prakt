SET SERVEROUTPUT ON;

CREATE TABLE ShortestRoute (
    City1 VARCHAR2(50),
    Country1 VARCHAR2(4),
    Province1 VARCHAR2(50)
    City2 VARCHAR2(50),
    Country2 VARCHAR2(4),
    Province2 VARCHAR2(50),
    Distance NUMBER
);

CREATE OR REPLACE PROCEDURE (
    Code1 Number,
    Code2 Number
)
IS
-- ...
BEGIN
    CREATE TABLE Vertices AS (
        (SELECT Code1 FROM Distances)
        UNION
        (SELECT Code2 FROM Distances)
        );
    ALTER TABLE Vertices ADD Distance NUMBER;
    ALTER TABLE Vertices ADD Predecessor VARCHAR2(50);
    CREATE TABLE Unvisited AS (
        (SELECT Code1 FROM Distances)
        UNION
        (SELECT Code2 FROM Distances)
        );

    DROP TABLE Vertices;
    DROP TABLE Unvisited;
END;
/
