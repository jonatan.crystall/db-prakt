-- Add density column
ALTER TABLE country
ADD (
  density REAL
);

-- Fill density column completely
CREATE OR REPLACE PROCEDURE fillDensity IS
--  den REAL := 0;
BEGIN
  FOR c in (SELECT * FROM country)
  LOOP
    UPDATE country
    SET density = c.population / c.area
    WHERE code = c.code;
  END LOOP;
END;
/

EXECUTE fillDensity();

-- 4.4.1 Update density in case of area or population updates
CREATE TABLE temp_country_update
(
  country VARCHAR2(4),
  area number,
  population number
);

CREATE OR REPLACE TRIGGER prepareCountryDensityUpdate
BEFORE UPDATE OF area, population ON country
FOR EACH ROW
BEGIN
	INSERT INTO temp_country_update
  VALUES(:OLD.code, :NEW.area, :NEW.population);
END;
/

CREATE OR REPLACE TRIGGER performCountryDensityUpdate
AFTER UPDATE OF area, population ON country
BEGIN
	UPDATE country 
  SET density = (SELECT population/area FROM temp_country_update WHERE country = code)
  WHERE code IN (SELECT country FROM temp_country_update);
  
  DELETE FROM temp_country_update;
END;
/

-- 4.4.2 Create a countrypops entry on population updates
CREATE OR REPLACE TRIGGER logCountryPopsUpdate
AFTER UPDATE OF population ON country
FOR EACH ROW
DECLARE
  pops_exist number(1);
BEGIN
  SELECT CASE
    WHEN EXISTS (
      SELECT * 
      FROM countryPops 
      WHERE country = :OLD.code 
        AND year = (select to_char(sysdate, 'YYYY') from dual)
      ) 
    THEN 1
    ELSE 0
    END into pops_exist
  FROM DUAL;
  
  IF pops_exist = 1 
  THEN
    UPDATE countryPops
    SET population = :NEW.population
    WHERE country = :OLD.code AND year = (select to_char(sysdate, 'YYYY') from dual);
  ELSE
    INSERT INTO countryPops
    VALUES(:OLD.code, (select to_char(sysdate, 'YYYY') from dual), :NEW.population);
  END IF;
  
  UPDATE population
  SET POPULATION_GROWTH = :NEW.population / :OLD.population
  WHERE country = :OLD.code;
END;
/
