/* Loesche alle Eintraege in Sea, in dem keine sinnvolle located Information
vorhanden ist. Beim Loeschen eines Meeres werden die referenzierten Werte durch
die Constraints aus Aufgabe 3.2 automatisch auf `null` gesetzt. */
create or replace trigger delSea
after   delete
	on sea
begin
	delete from located where river is null and sea is null and lake is null;
end;
/

/* Dieser Trigger kommt ohne Constraints aus, da er manuell fuer alle geloeschten
Seen zuerst die Referenz in located auf `null` setzt und anschliessend reduntante
Eintraege loescht. */
create or replace trigger delLake
after   delete
	on lake
	for each row
begin	
	update located set lake=null where lake=:old.name;
	delete from located where sea is null and river is null and lake is null;
end;
/

create or replace trigger delRiver
after   delete
	on river
	for each row
begin
	update located set river=null where river=:old.name;
	delete from located where sea is null and river is null and lake is null;
end;
/
