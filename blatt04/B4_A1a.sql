
set serveroutput on;
-- Berechnet die Gesammtlaenge eines Flusssystems
create or replace function riverlength (riv VARCHAR2)
return number
   
is
	-- relutierende laenge
   len number := 0;

begin
	-- nvl command := wenn length null => set length 0
	select nvl(length,0) into len 
	from river 
	where name = riv;

--dbms_output.put_line(len);
-- eingebettete Cursor For Loop
for entry in 
		
		-- loop fuer jeden Eintrag in River
		(
		select * 
		from river 
		where river = riv 
		and length is not null
		)
loop
			--dbms_output.put_line(entry.name);
		-- rekurisiver Summierung der Flusslaenge
    	len:=len+riverlength(entry.name);
end loop;
	--Gesammtlaenge des Flusssystems
	return len;
end;
/
