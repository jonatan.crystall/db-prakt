SET SERVEROUTPUT ON;

-- Symmetric Distances between airports (=> All possible edges)
CREATE OR REPLACE VIEW sDistances AS
    (SELECT Code1, Code2, Dist FROM Distances)
    UNION
    (SELECT Code2, Code1, Dist FROM Distances);

-- Temp table with distances etc. during calculation
CREATE TABLE nodeestimate (
    Code VARCHAR2(50),
    Distance NUMBER,
    Predecessor VARCHAR2(50),
    Done NUMBER(1,0)
);

CREATE OR REPLACE PROCEDURE Dijkstra (
    Startcode VARCHAR2,
    Endcode VARCHAR2,
	Range NUMBER
)
IS
    currentfromcode VARCHAR2(50);
    currentdistance NUMBER;
    temp NUMBER;
BEGIN
    DELETE FROM nodeestimate;
    -- Fill nodeestimate with all nodes from sDistances
    INSERT INTO nodeestimate (Code, Distance, Predecessor, Done)
        SELECT distinct Code1, 9999999999, NULL, 0 from sDistances;
    -- Update estimate for startnode
    UPDATE nodeestimate SET Distance = 0 where Code = Startcode;

    LOOP
        currentfromcode := NULL;
        -- Select node with shortest estimated distance
        select count(*) into temp
        FROM nodeestimate
            WHERE Done = 0;
            -- AND Distance IS NOT NULL;
        IF temp > 0
        then
            SELECT Code, Distance INTO currentfromcode, currentdistance
                FROM nodeestimate
                WHERE Done = 0
                -- AND Distance IS NOT NULL
                AND ROWNUM = 1
                ORDER BY Distance NULLS LAST;
        end if;
        
        -- no nodes left
        IF currentfromcode IS NULL OR currentfromcode = Endcode
        THEN
            EXIT;
        END IF;
        
        -- current node done
        UPDATE nodeestimate SET Done = 1 WHERE Code = currentfromcode;
        
        -- Update estimated Distances of neighbours
        -- UPDATE nodeestimate n
        --     SET n.Distance = currentdistance + (
        --             SELECT s.Dist FROM sDistances s
        --             WHERE s.Code2 = n.Code
        --             AND n.done = 0
        --             AND s.Code1 = currentfromcode
        --             AND (currentdistance + s.Dist) < n.Distance
        --             AND s.Dist <= Range
        --         ),
        --     n.Predecessor = currentfromcode
        --     ;
        UPDATE nodeestimate n
            SET n.Distance = currentdistance + (select s.Dist from sDistances s
                                                where n.Code = s.Code2
                                                and s.Code1 = currentfromcode),
                n.Predecessor = currentfromcode
            where exists (select * from sDistances s
                where n.Code = s.Code2
                and n.Done = 0
                and s.Code1 = currentfromcode
                and (currentdistance + s.Dist) < n.Distance
                and s.Dist <= Range);
    END LOOP;

END;
/

-- SELECT  *
--     FROM nodeestimate, Distances
--     WHERE Code = Code2
--     AND Done = 0
--     AND Code1 = 'HAM'
--     AND (0 + Dist) < Distance
--     AND Dist <= 12345;