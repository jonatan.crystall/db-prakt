import java.sql.*;
import java.util.ArrayList;
import java.util.List;

// TODO: Proper Exception Handling (maybe...)
public class Task01 {
    /**
     * Deletes a river and updates the DB accordingly
     */
    public static void deleteRiver(String riverToDelete) throws Exception {
        Connection conn = DriverManager.getConnection("jdbc:default:connection");
        // Set first column null
        ArrayList<PreparedStatement> setNullStatements = new ArrayList<PreparedStatement>();
        setNullStatements.add(updateableStatementHelper(conn, "select River from Lake where River = ?"));
        // Delete
        // For some reason this does not work on "select *" (error like "delete on read-only ResultSet") 
        ArrayList<PreparedStatement> deleteStatements = new ArrayList<PreparedStatement>();
        deleteStatements.add(updateableStatementHelper(conn, "select Name from River where Name = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select River from riverOnIsland where River = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select River from riverThrough where River = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select River from geo_river where River = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select River from geo_source where River = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select River from geo_estuary where River = ?"));
        // Delete if no other refs set, else set the first column to null
        ArrayList<PreparedStatement> otherStatements = new ArrayList<PreparedStatement>();
        otherStatements.add(updateableStatementHelper(conn, "select River, Lake, Sea from located where River = ?"));
        otherStatements.add(updateableStatementHelper(conn, "select River, Lake, Sea from islandIn where River = ?"));

        setRowsNull(setNullStatements, riverToDelete);
        deleteRows(deleteStatements, riverToDelete);
        deleteRowsOrNull(otherStatements, riverToDelete);
        
        for (PreparedStatement stmt : setNullStatements) {
            stmt.close();
        }
        for (PreparedStatement stmt : deleteStatements) {
            stmt.close();
        }
        for (PreparedStatement stmt : otherStatements) {
            stmt.close();
        }
        conn.close();
    }

    /**
     * Deletes a lake and updates the DB accordingly
     */
    public static void deleteLake(String lakeToDelete) throws Exception {
        Connection conn = DriverManager.getConnection("jdbc:default:connection");
        // Set first column null
        ArrayList<PreparedStatement> setNullStatements = new ArrayList<PreparedStatement>();
        setNullStatements.add(updateableStatementHelper(conn, "select Lake from River where Lake = ?"));
        // Delete
        // For some reason this does not work on "select *" (error like "delete on read-only ResultSet")
        ArrayList<PreparedStatement> deleteStatements = new ArrayList<PreparedStatement>();
        deleteStatements.add(updateableStatementHelper(conn, "select Name from Lake where Name = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select Lake from lakeOnIsland where Lake = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select Lake from riverThrough where Lake = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select Lake from geo_lake where Lake = ?"));
        // Delete if no other refs set, else set the first column to null
        ArrayList<PreparedStatement> otherStatements = new ArrayList<PreparedStatement>();
        otherStatements.add(updateableStatementHelper(conn, "select Lake, River, Sea from located where Lake = ?"));
        otherStatements.add(updateableStatementHelper(conn, "select Lake, River, Sea from islandIn where Lake = ?"));

        setRowsNull(setNullStatements, lakeToDelete);
        deleteRows(deleteStatements, lakeToDelete);
        deleteRowsOrNull(otherStatements, lakeToDelete);
        
        for (PreparedStatement stmt : setNullStatements) {
            stmt.close();
        }
        for (PreparedStatement stmt : deleteStatements) {
            stmt.close();
        }
        for (PreparedStatement stmt : otherStatements) {
            stmt.close();
        }
        conn.close();
    }

    /**
     * Deletes a sea and updates the DB accordingly
     */
    public static void deleteSea(String seaToDelete) throws Exception {
        Connection conn = DriverManager.getConnection("jdbc:default:connection");
        // Set first column null
        ArrayList<PreparedStatement> setNullStatements = new ArrayList<PreparedStatement>();
        setNullStatements.add(updateableStatementHelper(conn, "select Sea from River where Sea = ?"));
        // Delete
        // For some reason this does not work on "select *" (error like "delete on read-only ResultSet")
        ArrayList<PreparedStatement> deleteStatements = new ArrayList<PreparedStatement>();
        deleteStatements.add(updateableStatementHelper(conn, "select Name from Sea where Name = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select Sea from geo_sea where Sea = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select Sea1 from mergesWith where Sea1 = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select Sea2 from mergesWith where Sea2 = ?"));
        // Delete if no other refs set, else set the first column to null
        ArrayList<PreparedStatement> otherStatements = new ArrayList<PreparedStatement>();
        otherStatements.add(updateableStatementHelper(conn, "select Sea, Lake, River from located where Sea = ?"));
        otherStatements.add(updateableStatementHelper(conn, "select Sea, Lake, River from islandIn where Sea = ?"));

        setRowsNull(setNullStatements, seaToDelete);
        deleteRows(deleteStatements, seaToDelete);
        deleteRowsOrNull(otherStatements, seaToDelete);
        
        for (PreparedStatement stmt : setNullStatements) {
            stmt.close();
        }
        for (PreparedStatement stmt : deleteStatements) {
            stmt.close();
        }
        for (PreparedStatement stmt : otherStatements) {
            stmt.close();
        }
        conn.close();
    }

    /**
     * @return updateable and deleteable PreparedStatement
     */
    private static PreparedStatement updateableStatementHelper(Connection conn, String sql) throws Exception {
        return conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
    }

    /**
     * Sets the first column to 'null' for all rows in every passed PreparedStatement
     */
    private static void setRowsNull(List<PreparedStatement> setNullStatements, String keyToSetNull) throws Exception {
        for (PreparedStatement stmt : setNullStatements) {
            stmt.setString(1, keyToSetNull);
            ResultSet rset = stmt.executeQuery();
            while (rset.next()) {
                rset.updateNull(1);
                rset.updateRow();
            }
            rset.close();
        }
    }

    /**
     * Deletes every row for all passed PreparedStatements
     */
    private static void deleteRows(List<PreparedStatement> deleteStatements, String keyToDelete) throws Exception {
        for (PreparedStatement stmt : deleteStatements) {
            stmt.setString(1, keyToDelete);
            ResultSet rset = stmt.executeQuery();
            while (rset.next()) {
                rset.deleteRow();
            }
            rset.close();
        }
    }

    /**
     * Deletes all rows, where all columns from the query except the first are 'null', otherwise first column is set to 'null'
     * ATTENTION: Therefore the order of the select statement is very important!
     */
    private static void deleteRowsOrNull(List<PreparedStatement> otherStatements, String keyToOther) throws Exception {
        for (PreparedStatement stmt : otherStatements) {
            stmt.setString(1, keyToOther);
            ResultSet rset = stmt.executeQuery();
            int columnCount = rset.getMetaData().getColumnCount();
            while (rset.next()) {
                boolean allNull = true;
                for (int i = 2; allNull && i <= columnCount; i++) {
                    rset.getObject(i);
                    if(!rset.wasNull()) {
                        allNull = false;
                    }
                }
                if (allNull) {
                    rset.deleteRow();
                } else {
                    rset.updateNull(1);
                    rset.updateRow();
                }
            }
            rset.close();
        }
    }
}
