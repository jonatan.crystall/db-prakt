import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.sql.*;
import java.util.*;

public class QueryWindow extends JFrame {
    private static final String DB_URL = "jdbc:oracle:thin:@//oracle12c.informatik.uni-goettingen.de:1521/dbis";
    // Credentials
    private static final String USER = "dominickleppich";
    private static final String PASS = "xie3eLiW";

    // ------------------------------------------------------------

    private JTextArea queryText;
    private JTextArea resultAnswer;
    private JButton submitButton;
    private JTable resultTable;

    // ------------------------------------------------------------

    public QueryWindow() {
        super("JDBC Query Tool");

        queryText = new JTextArea("SELECT *\nFROM country");
        submitButton = new JButton("Submit");
        resultTable = new JTable();
        resultAnswer = new JTextArea();
        resultAnswer.setEditable(false);

        queryText.setPreferredSize(new Dimension(500, 200));
        submitButton.setVerticalAlignment(JButton.BOTTOM);

        submitButton.addActionListener(actionEvent -> performQuery(queryText.getText()));

        Box hBox = Box.createHorizontalBox();
        hBox.add(queryText);
        hBox.add(submitButton);

        add(hBox, BorderLayout.NORTH);
        JScrollPane resultTableScrollPane = new JScrollPane(resultTable);
        resultTableScrollPane.setPreferredSize(new Dimension(800, 600));
        add(resultTableScrollPane, BorderLayout.CENTER);
        add(resultAnswer, BorderLayout.SOUTH);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }

    private void performQuery(String query) {
        System.out.println("Performing Query:\n\n" + query);
        resultTable.setModel(new DefaultTableModel());
        resultAnswer.setText("");

        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);

            Statement stmt = conn.createStatement();

            ResultSet result = stmt.executeQuery(query);

            ResultSetMetaData meta = result.getMetaData();
            if (meta.getColumnCount() > 0) {
                DefaultTableModel model = buildTableModel(result);
                resultTable.setModel(model);
                resultAnswer.setText(model.getRowCount() + " resulting rows.");
            } else
                resultAnswer.setText(stmt.getUpdateCount() + " rows affected.");

            result.close();
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
            resultAnswer.setText(e.getMessage());
        }
    }

    public static DefaultTableModel buildTableModel(ResultSet rs) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<String>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        while (rs.next()) {
            Vector<Object> vector = new Vector<Object>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }

        System.out.println(data.size());

        return new DefaultTableModel(data, columnNames);

    }

    // ------------------------------------------------------------

    public static void main(String[] args) {
        new QueryWindow();
    }
}
