import javafx.application.*;
import javafx.collections.*;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

import javax.swing.table.*;
import java.io.*;
import java.sql.*;
import java.util.*;

public class FXQueryTool extends Application {
    private static final String DB_URL = "jdbc:oracle:thin:@//oracle12c.informatik.uni-goettingen.de:1521/dbis";
    // Credentials
    private static final String USER = "dominickleppich";
    private static final String PASS = "xie3eLiW";

    // ---------------------------------------------------------

    @FXML
    private TextArea query;

    @FXML
    private TextArea answerText;
    @FXML
    private TableView answerTable;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(FXQueryTool.class.getResource("FXQueryWindow.fxml"));
            Pane pane = loader.load();
            primaryStage.setTitle("JavaFX Query Tool");
            primaryStage.setScene(new Scene(pane));
            primaryStage.show();
        } catch (IOException e) {
            answerText.setText(e.getMessage());
        }
    }

    // ------------------------------------------------------------

    @FXML
    public void performQuery() {
        System.out.println("Performing Query:\n\n" + query);

        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);

            Statement stmt = conn.createStatement();

            ResultSet result = stmt.executeQuery(query.getText());
            ResultSetMetaData meta = result.getMetaData();

            answerTable.setItems(createResultList(result));

            result.close();
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            answerText.setText(e.getMessage());
        }
    }

    public static ObservableList<String> createResultList(ResultSet rs) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<String>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }

        return FXCollections.observableArrayList();
    }
}
