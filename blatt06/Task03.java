/*@lineinfo:filename=Task03*//*@lineinfo:user-code*//*@lineinfo:1^1*/import java.sql.*;
import oracle.sqlj.runtime.Oracle;

import sqlj.runtime.*;
import sqlj.runtime.ref.DefaultContext;

public class Task03 {
    public static void main(String[] args) throws Exception {
        Oracle.connect(Task03.class, "conf.props");

        /*@lineinfo:generated-code*//*@lineinfo:11^9*/

//  ************************************************************
//  SQLJ iterator declaration:
//  ************************************************************

class GdpIter
extends sqlj.runtime.ref.ResultSetIterImpl
implements sqlj.runtime.NamedIterator
{
  public GdpIter(sqlj.runtime.profile.RTResultSet resultSet)
    throws java.sql.SQLException
  {
    super(resultSet);
    codeNdx = findColumn("code");
    nameNdx = findColumn("name");
    populationNdx = findColumn("population");
    gdpNdx = findColumn("gdp");
    m_rs = (oracle.jdbc.OracleResultSet) resultSet.getJDBCResultSet();
  }
  private oracle.jdbc.OracleResultSet m_rs;
  public String code()
    throws java.sql.SQLException
  {
    return (String)m_rs.getString(codeNdx);
  }
  private int codeNdx;
  public String name()
    throws java.sql.SQLException
  {
    return (String)m_rs.getString(nameNdx);
  }
  private int nameNdx;
  public int population()
    throws java.sql.SQLException
  {
    int __sJtmp = m_rs.getInt(populationNdx);
    if (m_rs.wasNull()) throw new sqlj.runtime.SQLNullException(); else return __sJtmp;
  }
  private int populationNdx;
  public int gdp()
    throws java.sql.SQLException
  {
    int __sJtmp = m_rs.getInt(gdpNdx);
    if (m_rs.wasNull()) throw new sqlj.runtime.SQLNullException(); else return __sJtmp;
  }
  private int gdpNdx;
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:11^80*/
        GdpIter iter;
        double population = 0;
        double gdp = 0.;

        double targetGdp;
        /*@lineinfo:generated-code*//*@lineinfo:17^9*/

//  ************************************************************
//  #sql { select sum(gdp/2)  from Economy };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = sqlj.runtime.ref.DefaultContext.getDefaultContext(); if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sum(gdp/2)   from Economy";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0Task03",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   targetGdp = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:17^61*/
        double overallPop;
        /*@lineinfo:generated-code*//*@lineinfo:19^9*/

//  ************************************************************
//  #sql { select sum(population)  from Country };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = sqlj.runtime.ref.DefaultContext.getDefaultContext(); if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sum(population)   from Country";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1Task03",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   overallPop = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:19^67*/

        /*@lineinfo:generated-code*//*@lineinfo:21^9*/

//  ************************************************************
//  #sql iter = { select c.code, c.name, c.population, e.gdp from Country c join Economy e on c.code = e.Country where c.population is not null and e.gdp is not null
//              order by e.gdp/c.population desc };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = sqlj.runtime.ref.DefaultContext.getDefaultContext(); if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select c.code, c.name, c.population, e.gdp from Country c join Economy e on c.code = e.Country where c.population is not null and e.gdp is not null\n            order by e.gdp/c.population desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2Task03",theSqlTS);
   // execute query
   iter = new GdpIter(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2Task03",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:23^45*/
        
            while (iter.next() && Math.abs(gdp - targetGdp) > 0.0001) {
            if (gdp + iter.gdp() < targetGdp) {
                population += iter.population();
                gdp += iter.gdp();
                System.out.printf("%s: %d\n", iter.name(), iter.population());
            } else {
                double a = (targetGdp-gdp)/iter.gdp();
                population += Math.floor(a * iter.population());
                gdp += a * iter.gdp();
                System.out.printf("%s: %d\n", iter.name(), (int) Math.floor(a*iter.population()));

            }
        }
        System.out.printf("\n%.2f%% der Bevölkerung machen 50%% des BIP aus.\n", (population/overallPop*100));
    }
}/*@lineinfo:generated-code*/