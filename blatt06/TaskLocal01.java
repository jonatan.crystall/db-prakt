import java.sql.*;
import java.io.FileInputStream;
import java.util.Scanner;
import java.util.Properties;
import java.util.ArrayList;
import java.util.List;

// TODO: Stored Procedure, proper Exception Handling (maybe...)
public class TaskLocal01 {
    public static void main(String[] args) throws Exception {
        // DB conneciont
        DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        Properties conf = new Properties();
        conf.load(new FileInputStream("conf.props"));
        Connection conn = DriverManager.getConnection(conf.getProperty("url"), conf);

        // Console input
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter what to delete (River, Lake, Sea):");
        String operation = sc.next();
        String request;
        switch (operation) {
            case "River":
                System.out.println("Enter a River to delete:");
                request = sc.next();
                deleteRiver(request, conn);
                break;
            case "Lake":
                System.out.println("Enter a Lake to delete:");
                request = sc.next();
                deleteLake(request, conn);
                break;
            case "Sea":
                System.out.println("Enter a Sea to delete:");
                request = sc.next();
                deleteSea(request, conn);
                break;
            default:
                System.out.println("Unsupported type, exitting.");
                break;
        }
        conn.close();
    }

    /**
     * Deletes a river and updates the DB accordingly
     */
    public static void deleteRiver(String riverToDelete, Connection conn) throws Exception {
        // Connection conn = DriverManager.getConnection("jdbc:default:connection");
        // Set first column null
        ArrayList<PreparedStatement> setNullStatements = new ArrayList<>();
        setNullStatements.add(updateableStatementHelper(conn, "select River from Lake where River = ?"));
        // Delete
        // For some reason this does not work on "select *" (error like "delete on read-only ResultSet") 
        ArrayList<PreparedStatement> deleteStatements = new ArrayList<>();
        deleteStatements.add(updateableStatementHelper(conn, "select Name from River where Name = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select River from riverOnIsland where River = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select River from riverThrough where River = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select River from geo_river where River = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select River from geo_source where River = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select River from geo_estuary where River = ?"));
        // Delete if no other refs set, else set the first column to null
        ArrayList<PreparedStatement> otherStatements = new ArrayList<>();
        otherStatements.add(updateableStatementHelper(conn, "select River, Lake, Sea from located where River = ?"));
        otherStatements.add(updateableStatementHelper(conn, "select River, Lake, Sea from islandIn where River = ?"));

        setRowsNull(setNullStatements, riverToDelete);
        deleteRows(deleteStatements, riverToDelete);
        deleteRowsOrNull(otherStatements, riverToDelete);
        
        for (PreparedStatement stmt : setNullStatements) {
            stmt.close();
        }
        for (PreparedStatement stmt : deleteStatements) {
            stmt.close();
        }
        for (PreparedStatement stmt : otherStatements) {
            stmt.close();
        }
        // conn.close();
    }

    /**
     * Deletes a lake and updates the DB accordingly
     */
    public static void deleteLake(String lakeToDelete, Connection conn) throws Exception {
        // Connection conn = DriverManager.getConnection("jdbc:default:connection");
        // Set first column null
        ArrayList<PreparedStatement> setNullStatements = new ArrayList<>();
        setNullStatements.add(updateableStatementHelper(conn, "select Lake from River where Lake = ?"));
        // Delete
        // For some reason this does not work on "select *" (error like "delete on read-only ResultSet")
        ArrayList<PreparedStatement> deleteStatements = new ArrayList<>();
        deleteStatements.add(updateableStatementHelper(conn, "select Name from Lake where Name = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select Lake from lakeOnIsland where Lake = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select Lake from riverThrough where Lake = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select Lake from geo_lake where Lake = ?"));
        // Delete if no other refs set, else set the first column to null
        ArrayList<PreparedStatement> otherStatements = new ArrayList<>();
        otherStatements.add(updateableStatementHelper(conn, "select Lake, River, Sea from located where Lake = ?"));
        otherStatements.add(updateableStatementHelper(conn, "select Lake, River, Sea from islandIn where Lake = ?"));

        setRowsNull(setNullStatements, lakeToDelete);
        deleteRows(deleteStatements, lakeToDelete);
        deleteRowsOrNull(otherStatements, lakeToDelete);
        
        for (PreparedStatement stmt : setNullStatements) {
            stmt.close();
        }
        for (PreparedStatement stmt : deleteStatements) {
            stmt.close();
        }
        for (PreparedStatement stmt : otherStatements) {
            stmt.close();
        }
        // conn.close();
    }

    /**
     * Deletes a sea and updates the DB accordingly
     */
    public static void deleteSea(String seaToDelete, Connection conn) throws Exception {
        // Connection conn = DriverManager.getConnection("jdbc:default:connection");
        // Set first column null
        ArrayList<PreparedStatement> setNullStatements = new ArrayList<>();
        setNullStatements.add(updateableStatementHelper(conn, "select Sea from River where Sea = ?"));
        // Delete
        // For some reason this does not work on "select *" (error like "delete on read-only ResultSet")
        ArrayList<PreparedStatement> deleteStatements = new ArrayList<>();
        deleteStatements.add(updateableStatementHelper(conn, "select Name from Sea where Name = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select Sea from geo_sea where Sea = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select Sea1 from mergesWith where Sea1 = ?"));
        deleteStatements.add(updateableStatementHelper(conn, "select Sea2 from mergesWith where Sea2 = ?"));
        // Delete if no other refs set, else set the first column to null
        ArrayList<PreparedStatement> otherStatements = new ArrayList<>();
        otherStatements.add(updateableStatementHelper(conn, "select Sea, Lake, River from located where Sea = ?"));
        otherStatements.add(updateableStatementHelper(conn, "select Sea, Lake, River from islandIn where Sea = ?"));

        setRowsNull(setNullStatements, seaToDelete);
        deleteRows(deleteStatements, seaToDelete);
        deleteRowsOrNull(otherStatements, seaToDelete);
        
        for (PreparedStatement stmt : setNullStatements) {
            stmt.close();
        }
        for (PreparedStatement stmt : deleteStatements) {
            stmt.close();
        }
        for (PreparedStatement stmt : otherStatements) {
            stmt.close();
        }
        // conn.close();
    }

    /**
     * @return updateable and deleteable PreparedStatement
     */
    private static PreparedStatement updateableStatementHelper(Connection conn, String sql) throws Exception {
        return conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
    }

    /**
     * Sets the first column to 'null' for all rows in every passed PreparedStatement
     */
    private static void setRowsNull(List<PreparedStatement> setNullStatements, String keyToSetNull) throws Exception {
        for (PreparedStatement stmt : setNullStatements) {
            stmt.setString(1, keyToSetNull);
            ResultSet rset = stmt.executeQuery();
            while (rset.next()) {
                rset.updateNull(1);
                rset.updateRow();
            }
            rset.close();
        }
    }

    /**
     * Deletes every row for all passed PreparedStatements
     */
    private static void deleteRows(List<PreparedStatement> deleteStatements, String keyToDelete) throws Exception {
        for (PreparedStatement stmt : deleteStatements) {
            stmt.setString(1, keyToDelete);
            ResultSet rset = stmt.executeQuery();
            while (rset.next()) {
                rset.deleteRow();
            }
            rset.close();
        }
    }

    /**
     * Deletes all rows, where all columns from the query except the first are 'null', otherwise first column is set to 'null'
     * ATTENTION: Therefore the order of the select statement is very important!
     */
    private static void deleteRowsOrNull(List<PreparedStatement> otherStatements, String keyToOther) throws Exception {
        for (PreparedStatement stmt : otherStatements) {
            stmt.setString(1, keyToOther);
            ResultSet rset = stmt.executeQuery();
            int columnCount = rset.getMetaData().getColumnCount();
            while (rset.next()) {
                boolean allNull = true;
                for (int i = 2; allNull && i <= columnCount; i++) {
                    rset.getObject(i);
                    if(!rset.wasNull()) {
                        allNull = false;
                    }
                }
                if (allNull) {
                    rset.deleteRow();
                } else {
                    rset.updateNull(1);
                    rset.updateRow();
                }
            }
            rset.close();
        }
    }
}
