SELECT s1.city, s1.province, s1.country, c.longitude, c.latitude, s1.pop1, s2.pop2
FROM 
(
  SELECT city, province, country, population as pop1
  FROM citypops c11
  WHERE YEAR = 
  (
    SELECT MIN(year)
    FROM citypops c12
    WHERE c11.city = c12.city
      AND c11.country = c12.country
      AND c11.province = c12.province
      AND YEAR BETWEEN 1980 AND 2010
      GROUP BY city,province,country
  )
  AND country = 'USA'
) s1,
(
  SELECT city, province, country, population as pop2
  FROM citypops c21
  WHERE YEAR = 
  (
    SELECT MAX(year)
    FROM citypops c22
    WHERE c21.city = c22.city
      AND c21.country = c22.country
      AND c21.province = c22.province
      AND YEAR BETWEEN 1980 AND 2010
      GROUP BY city,province,country
  )
  AND country = 'USA'
) s2,
city c
WHERE s1.city = s2.city
  AND s1.province = s2.province
  AND s1.country = s2.country
  AND s2.city = c.name
  AND s2.province = c.province
  AND s2.country = c.country;