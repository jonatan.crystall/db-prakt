import java.awt.*;
import java.awt.geom.*;

public class City {
    private final String name;
    private final double longitude, latitude;
    private final long startPopulation, endPopulation;

    // ------------------------------------------------------------

    public City(final String country, final String province, final String name, final double longitude,
                final double latitude, final long startPopulation, final long endPopulation) {
        this.name = country + ", " + province + ", " + name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.startPopulation = startPopulation;
        this.endPopulation = endPopulation;

//        System.out.println(
//                "City created [" + this.name + "," + this.longitude + "," + this.latitude + "," + this.startPopulation +
//                "," + this.endPopulation);
    }

    // ------------------------------------------------------------

    public String getName() {
        return name;
    }

    /**
     * Return a normalized location between 0 and 1
     *
     * @return a normalized location
     */
    public Point2D getLocation() {
        return new Point2D.Double((longitude + 180.0) / 360.0, (latitude + 90.0) / 180.0);
    }

    public double getSize() {
        return Math.log10(endPopulation) * 1.0;
    }

    public Color getColor() {
        double value = (double) endPopulation / startPopulation;
//        System.out.println(value);

        int r = (value <= 1.0) ? 255 : (int) Math.round(255 - 255*(value-1.0));
        int g = (value <= 1.0) ? (int) Math.round(255 - 255*(1.0-value)) : 255;
        int b = 0;

        if (r < 0)
            r = 0;
        if (r > 255)
            r = 255;
        if (g < 0)
            g = 0;
        if (g > 255)
            g = 255;

        return new Color(r, g, b);
    }
}
