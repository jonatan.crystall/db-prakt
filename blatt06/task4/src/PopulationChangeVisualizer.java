import javax.swing.*;
import java.awt.*;
import java.awt.geom.*;
import java.sql.*;
import java.util.*;
import java.util.List;

public class PopulationChangeVisualizer extends JFrame {
    private static final String CITY_POP_QUERY = "SELECT s1.city, s1.province, s1.country, c.longitude, c.latitude, s1.pop1, s2.pop2\n" +
                                                 "FROM \n" + "(\n" +
                                                 "  SELECT city, province, country, population as pop1\n" +
                                                 "  FROM citypops c11\n" + "  WHERE YEAR = \n" + "  (\n" +
                                                 "    SELECT MIN(year)\n" + "    FROM citypops c12\n" +
                                                 "    WHERE c11.city = c12.city\n" +
                                                 "      AND c11.country = c12.country\n" +
                                                 "      AND c11.province = c12.province\n" +
                                                 "      AND YEAR BETWEEN 1980 AND 2010\n" +
                                                 "      GROUP BY city,province,country\n" + "  )\n" +
                                                 "  ) s1,\n" + "(\n" +
                                                 "  SELECT city, province, country, population as pop2\n" +
                                                 "  FROM citypops c21\n" + "  WHERE YEAR = \n" + "  (\n" +
                                                 "    SELECT MAX(year)\n" + "    FROM citypops c22\n" +
                                                 "    WHERE c21.city = c22.city\n" +
                                                 "      AND c21.country = c22.country\n" +
                                                 "      AND c21.province = c22.province\n" +
                                                 "      AND YEAR BETWEEN 1980 AND 2010\n" +
                                                 "      GROUP BY city,province,country\n" + "  )\n" +
                                                 "  ) s2,\n" + "city c\n" +
                                                 "WHERE s1.city = s2.city\n" + "  AND s1.province = s2.province\n" +
                                                 "  AND s1.country = s2.country\n" + "  AND s2.city = c.name\n" +
                                                 "  AND s2.province = c.province\n" + "  AND s2.country = c.country";

    private static final String DB_URL = "jdbc:oracle:thin:@//oracle12c.informatik.uni-goettingen.de:1521/dbis";
    // Credentials
    private static final String USER = "dominickleppich";
    private static final String PASS = "xie3eLiW";

    // ------------------------------------------------------------

    public static void main(String[] args) {
        new PopulationChangeVisualizer();
    }

    // ------------------------------------------------------------

    private VisualizePanel panel;
    private List<City> cities;

    public PopulationChangeVisualizer() {
        super("Population change visualizer");

        panel = new VisualizePanel();
        add(panel);

        // Get city data
        cities = getCityData();
        repaint();

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        pack();
        setVisible(true);
    }

    private List<City> getCityData() {
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);

            Statement stmt = conn.createStatement();

            ResultSet result = stmt.executeQuery(CITY_POP_QUERY);

            List<City> resultCities = new LinkedList<>();
            while (result.next()) {
                City c = new City(result.getString("country"), result.getString("province"), result.getString("city"),
                        result.getDouble("longitude"), result.getDouble("latitude"), result.getLong("pop1"),
                        result.getLong("pop2"));
                resultCities.add(c);
            }

            result.close();
            stmt.close();
            conn.close();

            return resultCities;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }



    // ------------------------------------------------------------

    class VisualizePanel extends JPanel {
        public VisualizePanel() {
            setPreferredSize(new Dimension(1500, 750));
        }

        @Override
        protected void paintComponent(Graphics g) {
            Graphics2D g2d = (Graphics2D) g;
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2d.setColor(Color.BLACK);
            g2d.fill(new Rectangle2D.Double(0, 0, getWidth(), getHeight()));
            drawGrid(g2d);
            render(g2d);
        }

        private void drawGrid(Graphics2D g) {
            g.setColor(Color.WHITE);
            g.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2);
        }

        private void render(Graphics2D g) {
            if (cities == null)
                return;

            for (City c : cities)
                drawCity(g, c);
        }

        private void drawCity(Graphics2D g, City c) {
            g.setColor(c.getColor());

            Point2D normalizedPoint = c.getLocation();
            Point2D point = new Point2D.Double(normalizedPoint.getX() * getWidth(),
                    getHeight() - normalizedPoint.getY() * getHeight());
            double size = c.getSize();
            Shape cityShape = new Ellipse2D.Double(point.getX() - size / 2, point.getY() - size / 2, size, size);
            g.fill(cityShape);
//            g.setColor(Color.BLACK);
//            g.draw(cityShape);

//            System.out.println("Drawing city: " + c.getName() + " @ " + normalizedPoint + " : " + point);
        }
    }
}
