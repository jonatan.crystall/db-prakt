import java.sql.*;
import java.io.FileInputStream;
import java.util.Scanner;
import java.util.Properties;
import java.util.ArrayList;

public class Spielkram {
    public static void main(String[] args) throws Exception {
        // DB conneciont
        DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        Properties conf = new Properties();
        conf.load(new FileInputStream("conf.props"));
        Connection conn = DriverManager.getConnection(conf.getProperty("url"), conf);

        // Console input
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a City (will change change the cities population to 42!):");
        String request = sc.next();

        System.out.println("Name Country Population");
        ArrayList<String> cities = getCities(request, conn);
        for (String city : cities) {
            System.out.println(city);
        }
        conn.close();
    }

    static ArrayList<String> getCities(String name, Connection conn) throws Exception {
        Statement stmt = conn.createStatement();
        PreparedStatement fetchCities = conn.prepareStatement("select Name, Country, Population from City where Name = ?", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        fetchCities.setString(1, name);
        ResultSet rset = fetchCities.executeQuery();
        // ResultSet rset = stmt.executeQuery("select Name, Country, Population from City where Name = '" + name + "'");
        ArrayList<String> result = new ArrayList<>();
        while (rset.next()) {
            String cityName = rset.getString("Name");
            String country = rset.getString("Country");
            int population = rset.getInt("Population");
            rset.updateInt(3, 42);
            rset.updateRow();
            result.add(cityName + " " + country + " " + population);
        }
        rset.close();
        stmt.close();
        return result;
    }
}
