-- Create the base object types
start task01.sql;

-- Create organization object nested table
CREATE TYPE Member_Type AS OBJECT
(
  Country VARCHAR(4),
  Type VARCHAR(30)
);
/

CREATE TYPE Member_List_Type AS TABLE OF Member_Type;
/


CREATE OR REPLACE TYPE Organization_Type AS OBJECT
(
  Name VARCHAR2(255),
  Abbreviation VARCHAR2(12),
  Established DATE,
  hasHqIn REF City_Type,
  MEMBER PROCEDURE set_member_type(c IN VARCHAR, t IN VARCHAR),
  MEMBER FUNCTION get_member_type(c IN VARCHAR) RETURN VARCHAR,
  MEMBER FUNCTION get_members RETURN Member_List_Type
);
/

CREATE OR REPLACE TYPE BODY Organization_Type
IS
  MEMBER PROCEDURE set_member_type(c IN VARCHAR, t IN VARCHAR)
  IS
    bool number := 0;
  BEGIN
    FOR entry IN (SELECT * FROM ismember WHERE country = c AND organization = SELF.abbreviation)
    LOOP
      UPDATE ismember SET type = t WHERE country = c AND organization = SELF.abbreviation;
      bool := 1;
    END LOOP;
    IF bool = 0 THEN
      INSERT INTO ismember VALUES (c, SELF.abbreviation, t);
    END IF;
  END set_member_type;
  
  MEMBER FUNCTION get_member_type(c IN VARCHAR) RETURN VARCHAR
  IS
    result_type VARCHAR(30);
  BEGIN
    SELECT type 
    INTO result_type
    FROM ismember 
    WHERE country = c 
      AND organization = SELF.abbreviation;
    RETURN result_type;
  END get_member_type;
  
  MEMBER FUNCTION get_members RETURN Member_List_Type
  IS
    result_table Member_List_Type := Member_List_Type();
  BEGIN
    FOR entry IN (SELECT country, type FROM ismember WHERE organization = SELF.abbreviation)
    LOOP
      result_table.extend;
      result_table(result_table.count) := Member_Type(entry.country, entry.type);
    END LOOP;
    return result_table;
  END get_members;
END;
/

-- Create table for Organization object type
CREATE TABLE OROrganization OF Organization_Type;

-- Fill table for Organization object type
INSERT INTO OROrganization
SELECT Organization_Type(o.name, o.abbreviation, o.established, REF(c))
FROM organization o LEFT OUTER JOIN ORCity c ON o.city = c.name AND o.country = c.country.code AND o.province = c.province.name;

-- Static functions..
CREATE OR REPLACE FUNCTION get_member_type(c IN VARCHAR, a IN VARCHAR) RETURN VARCHAR
IS
  result_type VARCHAR(30);
BEGIN
  SELECT o.get_member_type(c)
  INTO result_type
  FROM OROrganization o
  WHERE o.abbreviation = a;
  return result_type;
END;
/

CREATE OR REPLACE PROCEDURE make_member_type(c IN VARCHAR, a IN VARCHAR, t IN VARCHAR)
IS
  org Organization_Type;
BEGIN
  SELECT VALUE(o)
  INTO org
  FROM OROrganization o
  WHERE o.abbreviation = a;
  org.set_member_type(c, t);
END;
/


-- TESTS
-- Get all members of the European Union
SELECT o.get_members()
FROM OROrganization o
WHERE abbreviation = 'EU';

-- Get member type of Germany in European Union
SELECT o.get_member_type('D')
FROM OROrganization o
WHERE abbreviation = 'EU';

SELECT get_member_type('D', 'EU')
FROM dual;

-- Test make member type static procedure
SELECT get_member_type('USA', 'EU')
FROM dual;

EXECUTE make_member_type('USA', 'EU', 'just spying');

-- Some queries
-- 1. Number of citizens of cities, which are headquarter of an organization
SELECT SUM(pop)
FROM
(
  SELECT distinct o.hashqin.name as name, o.hashqin.population as pop
  FROM OROrganization o
);
-- Result: 174352802

-- 2. The names of all EU members with more than 10.000.000 citizens
SELECT c.name
FROM OROrganization o, TABLE (o.get_members()) m, country c
WHERE o.abbreviation = 'EU'
  AND c.code = m.country
  AND c.population > 10000000;
-- Result: 14 Entries
--    Greece
--    France
--    Spain
--    Czech Republic
--    Germany
--    Italy
--    Poland
--    Belgium
--    Netherlands
--    Romania
--    Portugal
--    Turkey
--    United Kingdom
--    United States
