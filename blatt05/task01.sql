drop type Country_Type force;
drop type City_Type force;
drop type Province_Type force;

create or replace type Country_Type
/
create or replace type Province_Type
/

create or replace type City_Type as object
(
	Name		varchar2(50),
	Country		ref Country_Type,
	Province	ref Province_Type,
	Population	number,
	Coordinates	GeoCoord,
	Elevation	number
);
/

create or replace type Country_Type as object
(
	Name		varchar2(50),
	Code		varchar2(4),
	Capital		ref City_Type,
	Area		number,
	Population	number,
	member function pop_growth
		return number
);
/

create or replace type Province_Type as object
(
	Name		varchar2(50),
	Country		ref Country_Type,
	Population	number,
	Area		number,
	Capital		ref City_Type
);
/

create or replace type body Country_Type
as
	member function pop_growth
		return number
	is
	cursor pops
		(pop_country varchar2)
		is select Population, Year
		from countrypops
		where Country = pop_country
		order by year desc;
	current_pop	number;
	last_pop	number;
	current_year	number;
	last_year	number;
	begin
		open pops(self.Code);
		fetch pops into current_pop, current_year;
		if pops%FOUND
		then
			fetch pops into last_pop, last_year;
		else
			close pops;
			return 0;
		end if;
		if pops%FOUND
		then
			close pops;
			return (current_pop - last_pop)/(current_year - last_year);
		end if;
		close pops;
		return 0;
	end;
end;
/
	

create table ORCity of City_Type;
create table ORProvince of Province_Type;
create table ORCountry of Country_Type;

insert into orcountry
select country_type(Name, Code, NULL, Area, Population)
from country;

insert into ORProvince
select Province_Type(pr.Name, ref(co), pr.Population, pr.Area, NULL)
from Province pr, ORCountry co
where pr.Country = co.Code;

insert into ORCity
select City_Type(ci.Name, ref(co), ref(pr), ci.Population, GeoCoord(ci.Latitude, ci.Longitude), ci.Elevation)
from City ci, ORCountry co, ORProvince pr
where ci.Country = co.Code
and ci.Province = pr.Name;

update ORProvince pr1
set Capital = (
	select ref(ci)
	from ORCity ci, Province pr2
	where pr1.Name = pr2.Name
	and pr1.Country.Code = pr2.Country	
	and ci.Name = pr2.Capital
	and ci.Province.Name = pr2.CapProv
	and ci.Country.Code = pr2.Country
	-- Super langsam, dafür sauber, schneller aber eklig:
	and rownum = 1
	-- and ci.Province.Country.Code = pr2.Country
);

update ORCountry co1
set Capital = (
	select ref(ci)
	from ORCity ci, Country co2
	where co1.Code = co2.Code
	and ci.Name = co2.Capital
	and ci.Country.Code = co2.Code
	and ci.Province.Name = co2.Province
	-- Super langsam, dafür sauber, schneller aber eklig:
	and rownum = 1
	-- and ci.Province.Country.Code = co2.Code
);

select c.Name, c.Capital.Province.Population from ORCountry c;
