create or replace type Border_Type as object
(
	Country1	ref Country_Type,
	Country2	ref Country_Type,
	Length		number
);
/

-- Geht doch :P Geht aber nicht mit Referenzen als Primary Keys
-- Man gewinnt aber keinen Komfort über SBoders_RefV
-- War vermutlich auch nicht in der Aufgabe gemeint.
create or replace view SBorders_ObjV of Border_Type
with object oid (Country1.code, Country2.code)
as
(select ref(co1), ref(co2), length
	from ORCountry co1, ORCountry co2, Borders b
	where co1.Code = b.Country1
	and co2.Code = b.Country2)
union
(select ref(co1), ref(co2), length
	from ORCountry co1, ORCountry co2, Borders b
	where co1.Code = b.Country2
	and co2.Code = b.Country1)
;

create or replace view SBorders_RefV(Country1, Country2, Length)
as
(select ref(co1), ref(co2), length
	from ORCountry co1, ORCountry co2, Borders b
	where co1.Code = b.Country1
	and co2.Code = b.Country2)
union
(select ref(co1), ref(co2), length
	from ORCountry co1, ORCountry co2, Borders b
	where co1.Code = b.Country2
	and co2.Code = b.Country1)
;
