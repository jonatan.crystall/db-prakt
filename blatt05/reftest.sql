select deref(a).name from
(
	select ref(ci) as a
	from ORCity ci, Province pr2, ORProvince pr1
	where pr1.Name = pr2.Name
	and pr1.Country.Code = pr2.Country
	and ci.Name = pr2.Capital
	and ci.Province.Name = pr2.CapProv
	and ci.Country.Code = pr2.Country
	group by ref(ci), ref(pr1)
	having count(*) > 1
);
