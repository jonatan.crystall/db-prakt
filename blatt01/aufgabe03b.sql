select co.code, co.name, count(*)
from Country co join City c1 on co.code = c1.country
where c1.population > (
	select c2.population from City c2
	where c2.province = co.province and c2.name = co.capital and c2.country = co.code
)
group by co.code, co.name
order by count(*);
