select city, province
from located
where country = (select code from Country where name = 'France')
and (city,country,province) not in (
	select city, country,province
	from located
	where river is not null
);
