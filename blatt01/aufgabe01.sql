select Organization.name, sum(population)
from Organization join isMember on abbreviation = organization join country on isMember.country = code
group by Organization.name
order by sum(population) desc;
