SELECT ct.name, ct.province, ct.country,
COALESCE(
(SELECT name FROM country WHERE capital = ct.name AND province = ct.province),
(SELECT LISTAGG(abbreviation, ', ') WITHIN GROUP (ORDER BY name) FROM organization WHERE city = ct.name AND province = ct.province GROUP BY city, province)
) as CapOrHQ
FROM city ct, country c
WHERE ct.country = c.code
ORDER BY ct.name ASC;