SELECT (
(SELECT longitude
FROM city
WHERE name = 'Berlin')
- 
(SELECT longitude
FROM city
WHERE name = 'Auckland')
+
180)
* (24*60/360) as minutes_sunshine_berlin -- calculate minues per degree longitude
FROM dual;