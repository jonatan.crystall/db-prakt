select c1.name, c2.name, count(*) co
from geo_river gr1 join geo_river gr2 on gr1.river = gr2.river join Country c1 on c1.code = gr1.country join Country c2 on c2.code = gr2.country
where gr1.country < gr2.country
group by c1.name, c2.name
union
select c1.name, c2.name, 0 co
from borders b join Country c1 on b.country1 = c1.code join Country c2 on b.country2 = c2.code
where c1.code < c2.code and (c1.code, c2.code) not in (
	select c1.name, c2.name
	from geo_river gr1 join geo_river gr2 on gr1.river = gr2.river join Country c1 on c1.code = gr1.country join Country c2 on c2.code = gr2.country
	where gr1.country < gr2.country
)
order by co desc;
