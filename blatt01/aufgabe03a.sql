select co.name
from Country co join City c1 on co.code = c1.country and co.capital = c1.name and co.province = c1.province
where c1.population < (select max(population) from City c2 where c2.country = co.code);
