select sum(population)/sum(area) from Country
where code = 'DZ' or code = 'LAR'
	or code in (select country1 from borders where country2 = 'DZ' or country2 = 'LAR')
	or code in (select country2 from borders where country1 = 'DZ' or country1 = 'LAR');
