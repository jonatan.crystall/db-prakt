select pop/(ar-wuesten)
from (select sum(population) as pop from Country
	where code = 'DZ' or code = 'LAR'
	or code in (select country1 from borders where country2 = 'DZ' or country2 = 'LAR')
	or code in (select country2 from borders where country1 = 'DZ' or country1 = 'LAR')),
	(select sum(area) as ar from Country
	where code = 'DZ' or code = 'LAR'
	or code in (select country1 from borders where country2 = 'DZ' or country2 = 'LAR')
	or code in (select country2 from borders where country1 = 'DZ' or country1 = 'LAR')),
	(select sum(distinct d.area) as wuesten from geo_desert g join Desert d on g.desert = d.name
	where
	g.country in (select country1 from borders where country2 = 'DZ' or country2 = 'LAR')
	or g.country in (select country2 from borders where country1 = 'DZ' or country1 = 'LAR'));
