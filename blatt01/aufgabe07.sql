select distinct *
from (
	select distinct Island.name, Island.area from Island join islandIn on Island.name = islandIn.island
	where islandIn.sea is not NULL
	and Island.name in (
		select distinct locatedOn.island from located natural right outer join locatedOn
		where located.sea is NULL
	)
)
order by area desc;
