select sum(e1.percentage*c1.area/100/(select area from Continent where name = 'Asia'))*100 from encompasses e1 join Country c1 on e1.country = c1.code
where e1.country not in (
	select e2.country from encompasses e2 join Country c2 on e2.country = c2.code
		where e2.continent = 'Europe'
		or c2.name = 'Armenia' or c2.name = 'Georgia'
		or c2.name = 'Kazakhstan' or c2.name = 'Israel'
		or c2.name = 'Azerbaijan')
	and e1.percentage >= 50
	and e1.continent = 'Asia';
