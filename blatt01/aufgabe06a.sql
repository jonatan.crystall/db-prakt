SELECT *
FROM city
WHERE longitude = (
SELECT MAX(longitude)
FROM city
WHERE longitude <= (
  SELECT (longitude - 180)
  FROM city
  WHERE name = 'Auckland'
)
);