select c1.name, c2.name, count(*)
from geo_river gr1 join geo_river gr2 on gr1.river = gr2.river join Country c1 on c1.code = gr1.country join Country c2 on c2.code = gr2.country
where gr1.country < gr2.country
group by c1.name, c2.name
order by count(*) desc;
