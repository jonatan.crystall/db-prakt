select o.name from Organization o
where not exists (
	select * from Country c join Economy ec on c.code = ec.country
	where ec.GDP/c.population > 1/10
	and not exists (
		select * from isMember im
		where im.organization = o.abbreviation
		and im.country = c.code
	)
);
