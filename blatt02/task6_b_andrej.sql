SELECT c.name
FROM 

( -- alle Laender, die nicht an Kuesteliegen, und #Nachbarn, die nicht an Kueste liegen
    SELECT
        c.code, COUNT(b.country2) AS count
    FROM
        country c, symborders b
    WHERE
        c.code NOT IN (SELECT * FROM countrycoast)
        AND b.country1 = c.code
        AND b.country2 NOT IN (SELECT * FROM countrycoast)
    GROUP BY c.code
) noCoastalCountry, neighbors neigh, country c
  -- neighbors:= Tupel (Land, #Nachbarn)

WHERE
    neigh.country = noCoastalCountry.code
    -- #Nachbarn muss #Nachrmarn ohne Kueste sein
    AND  noCoastalCountry.count = neigh.count
    -- join mmit Country, fuer den Namen
    AND c.code = noCoastalCountry.code;
