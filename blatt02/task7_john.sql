select mountain, city
from (
	select m.name as mountain, c.name as city,
	sqrt(power((m.coordinates.longitude-c.longitude), 2) + power((m.coordinates.latitude-c.latitude), 2)) as distance,
	row_number() over(
		partition by m.name
		order by sqrt(power((m.coordinates.longitude-c.longitude), 2) + power((m.coordinates.latitude-c.latitude), 2))
	) as num
	from geo_mountain gm join mountain m on gm.mountain = m.name, city c
	where gm.country = 'R'
)
where num = 1
order by mountain;
