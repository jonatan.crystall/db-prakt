ALTER SESSION SET NLS_DATE_FORMAT = 'MM-DD-YYY';

SELECT p1.country, p2.country
FROM politics p1, politics p2
WHERE MONTHS_BETWEEN(p1.independence, p2.independence) BETWEEN 0 AND 6
  AND p1.country < p2.country
ORDER BY MONTHS_BETWEEN(p1.independence, p2.independence) ASC;