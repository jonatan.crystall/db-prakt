select sum(length)
from river
start with name = 'Amazonas'
connect by prior river = name;

select sum(length)
from river
start with name = 'Donau'
connect by prior river = name;

select sum(length)
from river
start with name = 'Zaire'
connect by prior river = name;
