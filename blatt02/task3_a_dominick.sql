SELECT DISTINCT country, organization
FROM isMember m0, sBorders sb
WHERE NOT EXISTS (SELECT *
                  FROM isMember m1, isMember m2
                  WHERE m1.organization=m2.organization
                   AND m1.type='member'
                   AND m2.type='member'
                   AND m0.country=m1.country
                   AND m0.organization=m1.organization
                   AND country2=m2.country)
 AND country=country1;