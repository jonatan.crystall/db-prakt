-- Geben  Sie  f�r  jede  Organisation  die  Gesamtl�nge  ihrer  Au�engrenzen  an 

SELECT organization, SUM(length)
FROM isMember im1, sBorders
WHERE country=country1
 -- benachbarte Laender aus sBorders ohne gleicher Organisation
 AND country2 NOT IN (SELECT im2.country
                      FROM isMember im2
                      WHERE im1.organization=im2.organization)
GROUP BY organization
ORDER BY 1;