SELECT *
FROM (SELECT name
      FROM country, (SELECT country1, SUM(length) AS sl
                     FROM sBorders
                     GROUP BY country1)
      WHERE code=country1
      ORDER BY sl/area DESC)
WHERE ROWNUM=1;