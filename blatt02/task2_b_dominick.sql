SELECT *
FROM (SELECT name
      FROM country, (SELECT country1, COUNT(country2) AS cc2
                     FROM sBorders
                     GROUP BY country1)
      WHERE code=country1
      ORDER BY cc2/area DESC)
WHERE ROWNUM=1;