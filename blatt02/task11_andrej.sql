                                --relative Flaeche EU-Kolonien zu Africa.area
select wasdependent, sum(area), sum(area/(select area from continent where name = 'Africa'))
from 
    (-- alle Europaeeischen Laender
    select 
        c.code
    from 
        country c, encompasses enc
    where 
        c.code = enc.country
        and enc.continent = 'Europe'
        and enc.percentage = 100
    ) euro, country c, encompasses enc, politics poli
where 
    -- alle Afrikanischen Laender mit EU-Dependency
    c.code = enc.country
    and enc.continent = 'Africa'
    and enc.percentage = 100
    and poli.country = c.code
    and poli.wasdependent = euro.code
group by 
    wasdependent order by 2 desc
;
