ALTER SESSION SET NLS_DATE_FORMAT = 'MM';

SELECT country, independence
FROM politics
WHERE EXTRACT(month FROM independence) BETWEEN '01' and '06'
ORDER BY country ASC;