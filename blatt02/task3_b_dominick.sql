SELECT c.name, o.name
FROM country c, organization o
WHERE NOT EXISTS (SELECT *
                  FROM isMember im
                  WHERE code=im.country
                   AND organization=o.abbreviation)
-- AND type='member')
 AND NOT EXISTS ((SELECT country2
                  FROM sBorders
                  WHERE c.code=country1)
                 MINUS
                 (SELECT country
                  FROM isMember
                  WHERE organization=o.abbreviation
                 --  AND type='member'
));
                         