create or replace view Country_new as
	select name, code, capital, province, area
	from Country;

create view Country_v (name, code, capital, province, area, population) as
	select c.name, c.code, c.capital, c.province, c.area, p.population
	from Country c,
	(
		select sum(Province.population) population, Province.country country from Province
		group by Province.country
	) p
	where p.country = c.code;
