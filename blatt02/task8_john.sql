create or replace view neighbours as
	select country2 as country from sborders
	where country1 = (select country from city where name = 'Khartoum')
	union
	select country from city where name = 'Khartoum';

select sum(floor(population/100000)) as participants 
from encompasses join country on code = country 
where continent = 'Africa'
and country not in (select * from neighbours);

select
	(select sum(floor(population/100000)) 
	from encompasses join country on code = country 
	where continent = 'Africa'
	and country not in (select * from neighbours)
	and population/area <= 60)
	+ (select sum(floor(population/200000)) 
	from encompasses join country on code = country 
	where continent = 'Africa'
	and country not in (select * from neighbours)
	and population/area > 60) as participants
from dual;
