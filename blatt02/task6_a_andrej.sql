SELECT
    enc.continent, COUNT(c.code)
FROM
    country c, encompasses enc
WHERE
    c.code = enc.country
    AND enc.percentage = 100
    -- Staedte, die nicht an der Kueste liegen
    AND c.code NOT IN (SELECT * FROM countrycoast)
GROUP BY enc.continent
;
