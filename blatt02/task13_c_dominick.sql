ALTER SESSION SET NLS_DATE_FORMAT = 'DD. Mon';

SELECT country, independence
FROM politics
ORDER BY country ASC;