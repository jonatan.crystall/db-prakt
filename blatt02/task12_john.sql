create or replace view City_v (name, country, province, population, coordinates, elevation) as
	select name, country, province, population, GeoCoord(latitude, longitude), elevation
	from City;

select c.name from City_v c where c.coordinates.latitude >= 0;
