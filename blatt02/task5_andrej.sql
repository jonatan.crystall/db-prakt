-- Geben  Sie  alle  Paare  von  L�ndern  in  Europa  aus,  die  an  die  gleiche  Menge  von  Meeren  angrenzen

SELECT t1.cc as country1, t2.cc as country2
-- FROM benutzt 2 Tabellen t1, t2: europaeische Laender, mit Anzahl ihrer Meeren
FROM
    (
    SELECT
        enc.country as cc, count(s.sea) as count
    FROM
			-- Tupel (Land, Meer)
       encompasses enc, (SELECT country, sea FROM geo_sea GROUP BY country, sea) s
    -- Alle Laender, die 100% in EU liegen
    WHERE
        enc.continent = 'Europe'
        AND enc.percentage = 100
        AND s.country = enc.country
    GROUP BY
        enc.country
    ORDER BY
        enc.country
    ) t1,
    (
    SELECT
        enc.country as cc, count(s.sea) as count
    FROM
        encompasses enc, (SELECT country, sea FROM geo_sea GROUP BY country, sea) s
    WHERE
        enc.continent = 'Europe'
        AND enc.percentage = 100
        AND s.country = enc.country
    GROUP BY
        enc.country
    ORDER BY
        enc.country
    ) t2
-- vergleicht gleiche #Meere von verschiedenen Laendern
WHERE
    t1.count = t2.count
    AND t1.cc != t2.cc