ALTER SESSION SET NLS_DATE_FORMAT = 'DD MM YYYY';

--SELECT SUM(TO_SECONDS(independence)) / n -> TO_DATE 
SELECT MEDIAN(independence)
FROM politics p, encompasses e
WHERE continent = 'Europe'
  AND e.country = p.country
  AND independence IS NOT NULL;