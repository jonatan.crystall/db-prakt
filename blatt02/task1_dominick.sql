CREATE OR REPLACE VIEW sBorders (country1, country2, length) AS
 (SELECT country1, country2, length
  FROM borders)
 UNION
 (SELECT country2, country1, length
  FROM borders);