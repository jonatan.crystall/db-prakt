-- Country-City
ALTER TABLE Country
ADD(CONSTRAINT CountryCity FOREIGN KEY (Capital, Code, Province) REFERENCES City(Name, Country, Province));

-- Countrypops-Country
ALTER TABLE Countrypops
ADD(CONSTRAINT CpopsCountry FOREIGN KEY (Country) REFERENCES Country(Code) ON DELETE CASCADE);

-- City-Province
ALTER TABLE City
ADD(CONSTRAINT CityProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Citypops-City
ALTER TABLE Citypops
ADD(CONSTRAINT CtpopsCity FOREIGN KEY (City, Country, Province) REFERENCES City(Name, Country, Province) ON DELETE CASCADE);

-- Province-City
ALTER TABLE Province
ADD(CONSTRAINT ProvCity FOREIGN KEY (Capital, Country, Capprov) REFERENCES City(Name, Country, Province) ON DELETE CASCADE); --

-- Province-Country
ALTER TABLE Province
ADD(CONSTRAINT ProvCountry FOREIGN KEY (Country) REFERENCES Country(Code) ON DELETE CASCADE); --

-- Provpops-Province
ALTER TABLE Provpops
ADD(CONSTRAINT PpopsProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Organization-City
ALTER TABLE Organization
ADD(CONSTRAINT OrgCity FOREIGN KEY (City, Country, Province) REFERENCES City(Name, Country, Province) ON DELETE CASCADE); --
-- Ismember-Country
ALTER TABLE IsMember
ADD(CONSTRAINT IsmCountry FOREIGN KEY (Country) REFERENCES Country(Code) ON DELETE CASCADE);

-- Ismember-Organization
ALTER TABLE IsMember
ADD(CONSTRAINT IsmOrganization FOREIGN KEY (Organization) REFERENCES Organization(Abbreviation));

-- Borders(Country1, Country2)
ALTER TABLE Borders
ADD(CONSTRAINT BordersC1 FOREIGN KEY (Country1) REFERENCES Country(Code) ON DELETE CASCADE);

ALTER TABLE Borders
ADD(CONSTRAINT BordersC2 FOREIGN KEY (Country2) REFERENCES Country(Code) ON DELETE CASCADE);

-- Encompasses-Continent
ALTER TABLE Encompasses
ADD(CONSTRAINT EncompContinent FOREIGN KEY (Continent) REFERENCES Continent(Name) ON DELETE CASCADE);

-- Encompasses-Country
ALTER TABLE Encompasses
ADD(CONSTRAINT EncompCountry FOREIGN KEY (Country) REFERENCES Country(Code) ON DELETE CASCADE);

-- Economy-Country
ALTER TABLE Economy
ADD(CONSTRAINT EcoCountry FOREIGN KEY (Country) REFERENCES Country(Code) ON DELETE CASCADE);

-- Politics-Country
ALTER TABLE Politics
ADD(CONSTRAINT PolCountry FOREIGN KEY (Country) REFERENCES Country(Code) ON DELETE CASCADE);

-- Population-Country
ALTER TABLE Population
ADD(CONSTRAINT PopCountry FOREIGN KEY (Country) REFERENCES Country(Code) ON DELETE CASCADE);

-- Ethnicgroup-Country
ALTER TABLE Ethnicgroup
ADD(CONSTRAINT EthCountry FOREIGN KEY (Country) REFERENCES Country(Code) ON DELETE CASCADE); --

-- Religion-Country
ALTER TABLE Religion
ADD(CONSTRAINT RelCountry FOREIGN KEY (Country) REFERENCES Country(Code) ON DELETE CASCADE); --

-- Language-Country
ALTER TABLE Language
ADD(CONSTRAINT LanCountry FOREIGN KEY (Country) REFERENCES Country(Code) ON DELETE CASCADE); --

-- Geographie --
-- Lake
-- Lake-River
ALTER TABLE Lake
ADD(CONSTRAINT LakeRiver FOREIGN KEY (River) REFERENCES River(Name));

-- Geo_Lake-Province
ALTER TABLE Geo_Lake
ADD(CONSTRAINT GeoLakeProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_Lake-Lake
ALTER TABLE Geo_Lake
ADD(CONSTRAINT GeolakeLake FOREIGN KEY (Lake) REFERENCES Lake(Name) ON DELETE CASCADE);

-- River
-- River-River
ALTER TABLE River
ADD(CONSTRAINT RiverRiver FOREIGN KEY (River) REFERENCES River(Name));

-- River-Lake
ALTER TABLE River
ADD(CONSTRAINT RiverLake FOREIGN KEY (Lake) REFERENCES Lake(Name));

-- River-Sea
ALTER TABLE River
ADD(CONSTRAINT RiverSea FOREIGN KEY (Sea) REFERENCES Sea(Name));

-- RiverThrough-River
ALTER TABLE RiverThrough
ADD(CONSTRAINT RiverthRiver FOREIGN KEY (River) REFERENCES River(Name) ON DELETE CASCADE);

-- RiverThrough-Lake
ALTER TABLE RiverThrough
ADD(CONSTRAINT RiverthLake FOREIGN KEY (Lake) REFERENCES Lake(Name) ON DELETE CASCADE);

-- Geo_River-Province
ALTER TABLE Geo_River
ADD(CONSTRAINT GeoRiverProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_River-River
ALTER TABLE Geo_River
ADD(CONSTRAINT GeoRiverRiver FOREIGN KEY (River) REFERENCES River(Name) ON DELETE CASCADE);

-- Geo_Source-Province
ALTER TABLE Geo_Source
ADD(CONSTRAINT GeoSourceProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_Source-River
ALTER TABLE Geo_Source
ADD(CONSTRAINT GeoSourceRiver FOREIGN KEY (River) REFERENCES River(Name) ON DELETE CASCADE);

-- Geo_Estuary-Province
ALTER TABLE Geo_Estuary
ADD(CONSTRAINT GeoEstuaryProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_Estuary-River
ALTER TABLE Geo_Estuary
ADD(CONSTRAINT GeoEstuaryRiver FOREIGN KEY (River) REFERENCES River(Name) ON DELETE CASCADE);

-- Geo_Sea-Province
ALTER TABLE Geo_Sea
ADD(CONSTRAINT GeoSeaProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_Sea-Sea
ALTER TABLE Geo_Sea
ADD(CONSTRAINT GeoSeaSea FOREIGN KEY (Sea) REFERENCES Sea(Name) ON DELETE CASCADE);

-- Geo_Island-Province
ALTER TABLE Geo_Island
ADD(CONSTRAINT GeoIslandProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_Island-Island
ALTER TABLE Geo_Island
ADD(CONSTRAINT GeoIslIsland FOREIGN KEY (Island) REFERENCES Island(Name) ON DELETE CASCADE);

-- Geo_Mountain-Province
ALTER TABLE Geo_Mountain
ADD(CONSTRAINT GeoMProvince FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_Mountain-Mountain
ALTER TABLE Geo_Mountain
ADD(CONSTRAINT GeoMoMountain FOREIGN KEY (Mountain) REFERENCES Mountain(Name) ON DELETE CASCADE);

-- MountainOnIsland-Mountain
ALTER TABLE MountainOnIsland
ADD(CONSTRAINT MOIslandMountain FOREIGN KEY (Mountain) REFERENCES Mountain(Name) ON DELETE CASCADE);

-- MountainOnIsland-Island
ALTER TABLE MountainOnIsland
ADD(CONSTRAINT MOIslandIsland FOREIGN KEY (Island) REFERENCES Island(Name) ON DELETE CASCADE);

-- Geo_Desert-Province
ALTER TABLE Geo_Desert
ADD(CONSTRAINT GeoDesertProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_Desert-Desert
ALTER TABLE Geo_Desert
ADD(CONSTRAINT GeoDeDesert FOREIGN KEY (Desert) REFERENCES Desert(Name) ON DELETE CASCADE);

-- MergesWith(Sea1,Sea2)
ALTER TABLE MergesWith
ADD(CONSTRAINT MergesWS1 FOREIGN KEY (Sea1) REFERENCES Sea(Name) ON DELETE CASCADE);

ALTER TABLE MergesWith
ADD(CONSTRAINT MergeswS2 FOREIGN KEY (Sea2) REFERENCES Sea(Name) ON DELETE CASCADE);

-- IslandIn-Island
ALTER TABLE IslandIn
ADD(CONSTRAINT IslINIsl FOREIGN KEY (Island) REFERENCES Island(Name) ON DELETE CASCADE);

-- IslandIn-Sea
ALTER TABLE IslandIn
ADD(CONSTRAINT IslINSea FOREIGN KEY (Sea) REFERENCES Sea(Name) ON DELETE CASCADE);

--IslandIn-Lake
ALTER TABLE IslandIn
ADD(CONSTRAINT IslINLake FOREIGN KEY(Lake) REFERENCES Lake(Name) ON DELETE CASCADE):

--IslandIn-River
ALTER TABLE IslandIn
ADD(CONSTRAINT IslINRiv FOREIGN KEY (River) REFERENCES River(Name) ON DELETE CASCADE);

-- Located-City
ALTER TABLE Located
ADD(CONSTRAINT LocCity FOREIGN KEY (City, Country, Province) REFERENCES City(Name, Country, Province) ON DELETE CASCADE);

-- Located-River
ALTER TABLE Located
ADD(CONSTRAINT LocRiver FOREIGN KEY (River) REFERENCES River(Name) ON DELETE SET NULL);

-- Located-Lake
ALTER TABLE Located
ADD(CONSTRAINT LocLake FOREIGN KEY (Lake) REFERENCES Lake(Name) ON DELETE SET NULL);

-- Located-Sea
ALTER TABLE Located
ADD(CONSTRAINT LocSea FOREIGN KEY (Sea) REFERENCES Sea(Name) ON DELETE SET NULL);

-- LocatedOn-City
ALTER TABLE LocatedOn
ADD(CONSTRAINT LocOnCity FOREIGN KEY (City, Country, Province) REFERENCES City(Name, Country, Province) ON DELETE CASCADE);

-- LocatedOn-Island
ALTER TABLE LocatedOn
ADD(CONSTRAINT LocOnIsland FOREIGN KEY (Island) REFERENCES Island(Name) ON DELETE CASCADE);
