PROMPT Erzeuge neue Länder
PROMPT - Erzeuge neues Land "Padanien"
INSERT INTO Country
(name, code, capital, province)
VALUES
('Padanien', 'PAD', 'Milano', 'Lombardia');
PROMPT - Erzeuge neues Land "Venedig"
INSERT INTO Country
(name, code, capital, province)
VALUES
('Venedig', 'VEN', 'Venézia', 'Veneto');
PROMPT - Erzeuge neues Land "Pirates"
INSERT INTO Country
(name, code, capital, province)
VALUES
('Pirates', 'ARRR', 'Palermo', 'Sicilia');



PROMPT Erzeuge Hilfstabelle "ItalyProvinces".
CREATE TABLE ItalyProvinces
  ( Name        VARCHAR(50),
    NewCountry  VARCHAR(4)  CONSTRAINT ItalyNewCountryRefsCountry REFERENCES Country(code));
PROMPT - Erzeuge Einträge für Padanien Provinzen
INSERT INTO ItalyProvinces (Name, NewCountry)
(SELECT p.name, 'PAD'
FROM province p, city c
WHERE p.capital = c.name
  AND p.name = c.province
  AND p.country = 'I'
  AND c.country = 'I'
  AND c.latitude > 44
  AND p.name NOT IN ('Veneto', 'Friuli-Venezia Giulia', 'Trentino-Alto Adige'));
PROMPT - Erzeuge Einträge für Venedig Provinzen
INSERT INTO ItalyProvinces (Name, NewCountry)
(SELECT p.name, 'VEN'
FROM province p
WHERE p.country = 'I'
  AND p.name IN ('Veneto', 'Friuli-Venezia Giulia'));
PROMPT - Erzeuge Einträge für Österreich Provinzen
INSERT INTO ItalyProvinces (Name, NewCountry)
(SELECT p.name, 'A'
FROM province p
WHERE p.country = 'I'
  AND p.name IN ('Trentino-Alto Adige'));
PROMPT - Erzeuge Einträge für Piratenstaat Provinzen
INSERT INTO ItalyProvinces (Name, NewCountry)
(SELECT p.name, 'ARRR'
FROM province p
WHERE p.country = 'I'
  AND p.name IN ('Sicilia', 'Sardegna'));
PROMPT - Erzeuge Einträge für Vatikan Provinzen
INSERT INTO ItalyProvinces (Name, NewCountry)
(SELECT p.name, 'V'
FROM province p
WHERE p.country = 'I'
  AND p.name NOT IN
(SELECT name
FROM ItalyProvinces)
);



PROMPT Entferne direkte Referenzen auf Italien
DELETE FROM countryLocalName WHERE country = 'I';
DELETE FROM countryPops WHERE country = 'I';
DELETE FROM Economy WHERE country = 'I';
DELETE FROM Politics WHERE country = 'I';
DELETE FROM Population WHERE country = 'I';
DELETE FROM Religion WHERE country = 'I';
DELETE FROM EthnicGroup WHERE country = 'I';
DELETE FROM Borders WHERE country1 = 'I' or country2 = 'I';
DELETE FROM isMember WHERE country = 'I';
DELETE FROM encompasses WHERE country = 'I';



PROMPT Verschiebe Provinzen in die entsprechenden Länder
PROMPT - Verschiebe Provinzen nach Padanien
UPDATE province
SET country = 'PAD'
WHERE country = 'I'
  AND name in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT - Verschiebe Provinzen nach Venedig
UPDATE province
SET country = 'VEN'
WHERE country = 'I'
  AND name in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT - Verschiebe Provinzen nach Österreich
UPDATE province
SET country = 'A'
WHERE country = 'I'
  AND name in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT - Verschiebe Provinzen nach Piratenstaat
UPDATE province
SET country = 'ARRR'
WHERE country = 'I'
  AND name in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT - Verschiebe Provinzen nach Vatikan
UPDATE province
SET country = 'V'
WHERE country = 'I'
  AND name in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der Organization Tabelle
PROMPT Ändere Land zu Padanien in Tabelle Organization
UPDATE Organization
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle Organization
UPDATE Organization
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle Organization
UPDATE Organization
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle Organization
UPDATE Organization
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle Organization
UPDATE Organization
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der Airport Tabelle
PROMPT Ändere Land zu Padanien in Tabelle Airport
UPDATE Airport
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle Airport
UPDATE Airport
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle Airport
UPDATE Airport
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle Airport
UPDATE Airport
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle Airport
UPDATE Airport
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der City Tabelle
PROMPT Ändere Land zu Padanien in Tabelle City
UPDATE City
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle City
UPDATE City
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle City
UPDATE City
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle City
UPDATE City
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle City
UPDATE City
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der CityPops Tabelle
PROMPT Ändere Land zu Padanien in Tabelle CityPops
UPDATE CityPops
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle CityPops
UPDATE CityPops
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle CityPops
UPDATE CityPops
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle CityPops
UPDATE CityPops
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle CityPops
UPDATE CityPops
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der CityLocalName Tabelle
PROMPT Ändere Land zu Padanien in Tabelle CityLocalName
UPDATE CityLocalName
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle CityLocalName
UPDATE CityLocalName
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle CityLocalName
UPDATE CityLocalName
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle CityLocalName
UPDATE CityLocalName
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle CityLocalName
UPDATE CityLocalName
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der ProvPops Tabelle
PROMPT Ändere Land zu Padanien in Tabelle ProvPops
UPDATE ProvPops
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle ProvPops
UPDATE ProvPops
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle ProvPops
UPDATE ProvPops
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle ProvPops
UPDATE ProvPops
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle ProvPops
UPDATE ProvPops
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der ProvinceLocalName Tabelle
PROMPT Ändere Land zu Padanien in Tabelle ProvinceLocalName
UPDATE ProvinceLocalName
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle ProvinceLocalName
UPDATE ProvinceLocalName
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle ProvinceLocalName
UPDATE ProvinceLocalName
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle ProvinceLocalName
UPDATE ProvinceLocalName
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle ProvinceLocalName
UPDATE ProvinceLocalName
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der geo_sea Tabelle
PROMPT Ändere Land zu Padanien in Tabelle geo_sea
UPDATE geo_sea
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle geo_sea
UPDATE geo_sea
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle geo_sea
UPDATE geo_sea
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle geo_sea
UPDATE geo_sea
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle geo_sea
UPDATE geo_sea
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der geo_lake Tabelle
PROMPT Ändere Land zu Padanien in Tabelle geo_lake
UPDATE geo_lake
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle geo_lake
UPDATE geo_lake
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle geo_lake
UPDATE geo_lake
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle geo_lake
UPDATE geo_lake
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle geo_lake
UPDATE geo_lake
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der geo_river Tabelle
PROMPT Ändere Land zu Padanien in Tabelle geo_river
UPDATE geo_river
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle geo_river
UPDATE geo_river
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle geo_river
UPDATE geo_river
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle geo_river
UPDATE geo_river
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle geo_river
UPDATE geo_river
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der geo_source Tabelle
PROMPT Ändere Land zu Padanien in Tabelle geo_source
UPDATE geo_source
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle geo_source
UPDATE geo_source
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle geo_source
UPDATE geo_source
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle geo_source
UPDATE geo_source
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle geo_source
UPDATE geo_source
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der geo_estuary Tabelle
PROMPT Ändere Land zu Padanien in Tabelle geo_estuary
UPDATE geo_estuary
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle geo_estuary
UPDATE geo_estuary
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle geo_estuary
UPDATE geo_estuary
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle geo_estuary
UPDATE geo_estuary
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle geo_estuary
UPDATE geo_estuary
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der geo_island Tabelle
PROMPT Ändere Land zu Padanien in Tabelle geo_island
UPDATE geo_island
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle geo_island
UPDATE geo_island
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle geo_island
UPDATE geo_island
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle geo_island
UPDATE geo_island
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle geo_island
UPDATE geo_island
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der geo_mountain Tabelle
PROMPT Ändere Land zu Padanien in Tabelle geo_mountain
UPDATE geo_mountain
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle geo_mountain
UPDATE geo_mountain
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle geo_mountain
UPDATE geo_mountain
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle geo_mountain
UPDATE geo_mountain
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle geo_mountain
UPDATE geo_mountain
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der geo_desert Tabelle
PROMPT Ändere Land zu Padanien in Tabelle geo_desert
UPDATE geo_desert
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle geo_desert
UPDATE geo_desert
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle geo_desert
UPDATE geo_desert
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle geo_desert
UPDATE geo_desert
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle geo_desert
UPDATE geo_desert
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der located Tabelle
PROMPT Ändere Land zu Padanien in Tabelle located
UPDATE located
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle located
UPDATE located
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle located
UPDATE located
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle located
UPDATE located
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle located
UPDATE located
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Ändere die Länder der locatedOn Tabelle
PROMPT Ändere Land zu Padanien in Tabelle locatedOn
UPDATE locatedOn
SET country = 'PAD'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'PAD');
PROMPT Ändere Land zu Venedig in Tabelle locatedOn
UPDATE locatedOn
SET country = 'VEN'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'VEN');
PROMPT Ändere Land zu Österreich in Tabelle locatedOn
UPDATE locatedOn
SET country = 'A'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'A');
PROMPT Ändere Land zu Piratenstaat in Tabelle locatedOn
UPDATE locatedOn
SET country = 'ARRR'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'ARRR');
PROMPT Ändere Land zu Vatikan in Tabelle locatedOn
UPDATE locatedOn
SET country = 'V'
WHERE country = 'I'
  AND province in
(SELECT name
FROM ItalyProvinces
WHERE newcountry = 'V');



PROMPT Update area und population
UPDATE Country
SET population = (SELECT sum(population) from Province where country = Country.code),
    area = (SELECT sum(area) from Province where country = Country.code)
WHERE code in ('A', 'PAD', 'ARRR', 'V', 'VEN');



PROMPT Update encompasses
INSERT INTO encompasses(continent, country, percentage) VALUES ('Europe', 'PAD', 100);
INSERT INTO encompasses(continent, country, percentage) VALUES ('Europe', 'VEN', 100);
INSERT INTO encompasses(continent, country, percentage) VALUES ('Europe', 'ARR', 100);



PROMPT Update languages
CREATE TABLE absLang AS
(SELECT floor(language.percentage*country.population/100) AS speakers, language.name as language
FROM language JOIN country ON language.country = country.code WHERE country.code = 'I');

CREATE TABLE absLangA AS
(SELECT floor(language.percentage*country.population/100) AS speakers, language.name as language
FROM language JOIN country ON language.country = country.code WHERE country.code = 'A');

-- Mehr Deutsch in Österreich
UPDATE absLangA AS
SET speakers = speakers + (SELECT speakers FROM absLang WHERE language = 'German')
WHERE language = 'German';

-- Italienisch in Österreich
INSERT INTO absLangA AS
VALUES ((SELECT population FROM Province WHERE name = 'Trentino-Alto Adige') - (SELECT speakers FROM absLang WHERE language = 'German'), 'German');

-- Weniger Italienisch in Italien
UPDATE absLang
SET speakers = speakers - ((SELECT population FROM Province WHERE name = 'Trentino-Alto Adige') - (SELECT speakers FROM absLang WHERE language = 'German'));

-- Kein Deutsch mehr in Italien
DELETE FROM absLang WHERE language = 'German';

-- Sprachen in Österreich anpassen
-- FIXME weiterarbeiten

INSERT INTO language
VALUES ('A', 'Italian', ((SELECT population from Province where name = 'Trentino-Alto Adige')-(SELECT speakers FROM absLang WHERE language = 'German'))/((SELECT population FROM country WHERE code = 'A'))*100);

UPDATE Language
SET Percentage = (
  SELECT speakers*100/((SELECT population from Country where code = 'A') + (SELECT population from Province where Name = 'Trentino-Alto Adige'))
  from absLangA 
  where absLangA.name = lang.name
  )
WHERE country = 'A';

PROMPT Lösche Hilfstabelle ItalyProvinces
DROP TABLE ItalyProvinces;
