public class ItalyUpdate {
    public static final String[] NEW_COUNTRY_CODES = new String[]{"PAD", "VEN", "A", "ARRR", "V"};
    public static final String[] NEW_COUNTRY_NAMES = new String[]{"Padanien", "Venedig", "Österreich",
            "Piratenstaat", "Vatikan"};
    public static final String[] TABLES_TO_UPDATE = new String[]{"Airport", "City", "CityPops", "CityLocalName",
            "ProvPops", "ProvLocalName", "geo_sea", "geo_lake", "geo_river", "geo_source", "geo_estuary",
            "geo_island", "geo_mountain", "geo_desert", "located", "locatedOn"};

    public static void main(String[] args) {
        for (String table : TABLES_TO_UPDATE) {
            System.out.println("\n\n");
            updateTable(table);
        }
    }

    private static void updateTable(String table) {
        System.out.println("PROMPT Ändere die Länder der " + table + " Tabelle");
        for (int i = 0; i < NEW_COUNTRY_CODES.length; i++)
            updateTableForCountry(table, i);
    }

    private static void updateTableForCountry(String table, int countryIndex) {
        System.out.println("PROMPT Ändere Land zu " + NEW_COUNTRY_NAMES[countryIndex] + " in Tabelle " + table);
        System.out.println("UPDATE " + table + "\nSET country = '" + NEW_COUNTRY_CODES[countryIndex] + "'\nWHERE " +
                "country = 'I'\n  AND province in \n(SELECT name\nFROM ItalyProvinces\nWHERE newcountry = '" +
                NEW_COUNTRY_CODES[countryIndex] + "');");
    }
}
