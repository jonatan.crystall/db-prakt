CREATE OR REPLACE VIEW cstats AS
 SELECT code, government, gdp, population_growth AS popGrowth,
  infant_mortality AS infMort, population/area AS popDens
 FROM country, politics pol, economy e, population pop
 WHERE code=pol.country
  AND code=e.country
  AND code=pop.country;

SELECT c.code, gdp/population, infMort, popGrowth
FROM cstats cs, country c
WHERE c.code=cs.code
ORDER BY 2; --infMort, popGrowth nehmen ab

SELECT * FROM user_updatable_columns
WHERE Table_Name='CSTATS'; --popDens abgeleiteter Wert

UPDATE country
SET population=population*(1+(SELECT population_growth
                              FROM population
                              WHERE country=code)/100);

UPDATE cstats SET code='UK' WHERE code='GB'; --Aenderung wird nur in Country durchgefuehrt
