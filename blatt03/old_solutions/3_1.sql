-- Die zu dem neuen Staat "Europe" gehörenden Staaten
PROMPT Erstellen Tabelle EuropeUpdate für vorübergehende Speicherung der EU-Staaten 
CREATE TABLE EuropeUpdate
(name varchar2(255),
code varchar2(15) PRIMARY KEY,
capital varchar2(255),
province varchar2(255),
area number check (area>0),
population number check (population>0));


-- EuropeUpdate
PROMPT Fügen die Staaten hinzu
INSERT INTO EuropeUpdate(name, code, capital, province, area, population)
SELECT * 
FROM ((SELECT c.name, c.code, c.capital, c.province, c.area, c.population
	   FROM ismember i, country c
	   WHERE i.country = c.code
	   AND i.organization = 'EU'
	   AND i.type = 'member'
	   AND c.code != 'GR'
	   AND c.code != 'GB')
	   UNION
	  (SELECT name, code, capital, province, area, population
	   FROM country
	   WHERE code = 'CH'));

-- Country
PROMPT Passen die Tabelle Country an 
INSERT INTO country(name, code, capital, province, area, population)
VALUES ('Europe', 'EU', 
	(SELECT city FROM organization WHERE abbreviation = 'EU'),
	(SELECT c.name FROM organization o, country c WHERE o.abbreviation = 'EU' AND o.country = c.code), 
	(SELECT SUM(c.area) FROM country c WHERE c.code IN (SELECT code FROM EuropeUpdate)),
	(SELECT SUM(c.population) FROM country c WHERE c.code IN (SELECT code FROM EuropeUpdate)));


DELETE FROM country WHERE code IN (SELECT code FROM EuropeUpdate);

-- Province
PROMPT Passen die Tabelle Province an 
INSERT INTO province(name, country, area, population, capital, capprov)
SELECT e.name, 'EU', e.area, e.population, e.capital, e.name
FROM EuropeUpdate e;

UPDATE province
SET country = 'EU'
WHERE name = 'Sachsen' OR name = 'Bayern';


DELETE FROM province WHERE country IN (SELECT code FROM EuropeUpdate);

-- City
PROMPT Passen die Tabelle City an 
UPDATE city 
SET province = (SELECT name FROM EuropeUpdate WHERE EuropeUpdate.code=city.country), country='EU'
WHERE city.country IN (SELECT code FROM EuropeUpdate)
AND (city.province != 'Bayern'AND city.province != 'Sachsen');


UPDATE city
SET country = 'EU'
WHERE city.province='Sachsen' OR city.province='Bayern';


-- Countrypops
PROMPT Passen die Tabelle Countrypops an 
INSERT INTO countrypops(country, year, population)
VALUES ('EU', EXTRACT(YEAR FROM sysdate), (SELECT SUM(population) FROM EuropeUpdate));


-- Provpops
PROMPT Passen die Tabelle Provpops an 
INSERT INTO provpops(country, province, year, population)
SELECT 'EU', p.name, EXTRACT(YEAR FROM sysdate), e.population
FROM province p, EuropeUpdate e
WHERE p.country = 'EU'
AND p.name = e.name;


INSERT INTO provpops(country, province, year, population)
SELECT 'EU', p.name, EXTRACT(YEAR FROM sysdate), p.population
FROM province p
WHERE p.name = 'Sachsen' OR p.name = 'Bayern';


-- Citypops
PROMPT Passen die Tabelle Citypops an 
INSERT INTO citypops(city, country, province, year, population)
SELECT city.name, 'EU', city.province, EXTRACT(YEAR FROM sysdate), city.population
FROM city
WHERE city.country='EU';


-- Organization
PROMPT Passen die Tabelle Organization an 
UPDATE organization
SET country = 'EU', province = (SELECT province FROM city WHERE city.name = organization.city)
WHERE country IN (SELECT code FROM EuropeUpdate);


-- Ismember
PROMPT Passen die Tabelle Ismember an 
INSERT INTO ismember(organization, country, type)
SELECT DISTINCT i.organization, 'EU', 'member'
FROM ismember i
WHERE i.country IN (SELECT code FROM EuropeUpdate)
AND i.organization IN (SELECT organization 
	FROM ismember 
	WHERE country IN (SELECT code FROM europeupdate) GROUP BY organization 
	HAVING COUNT(country)>(SELECT COUNT(*) FROM europeupdate)/2);


DELETE FROM ismember
WHERE country IN (SELECT code FROM EuropeUpdate);


-- Geo_lake
PROMPT Passen die Tabelle Geo_lake an 
INSERT INTO geo_lake(lake, country, province)
SELECT DISTINCT g.lake, 'EU', e.name
FROM geo_lake g, EuropeUpdate e
WHERE g.country IN (SELECT code FROM EuropeUpdate)
AND g.country = e.code
AND g.province != 'Sachsen' AND g.province != 'Bayern';


UPDATE geo_lake
SET country = 'EU'
WHERE province = 'Sachsen' OR province = 'Bayern';


DELETE FROM geo_lake
WHERE country IN  (SELECT code FROM EuropeUpdate);


-- Geo_river
PROMPT Passen die Tabelle Geo_river an 
INSERT INTO geo_river(river, country, province)
SELECT DISTINCT g.river, 'EU', e.name
FROM geo_river g, EuropeUpdate e
WHERE g.country IN (SELECT code FROM EuropeUpdate)
AND g.country = e.code
AND g.province != 'Sachsen' AND g.province != 'Bayern';


UPDATE geo_river
SET country = 'EU'
WHERE province = 'Sachsen' OR province = 'Bayern';


DELETE FROM geo_river
WHERE country IN (SELECT code FROM EuropeUpdate);


-- Geo_source
PROMPT Passen die Tabelle Geo_source an 
INSERT INTO geo_source(river, country, province)
SELECT DISTINCT g.river, 'EU', e.name
FROM geo_source g, EuropeUpdate e
WHERE g.country IN (SELECT code FROM EuropeUpdate)
AND g.country = e.code
AND g.province != 'Sachsen' AND g.province != 'Bayern';


UPDATE geo_source
SET country = 'EU'
WHERE province = 'Sachsen' OR province = 'Bayern';


DELETE FROM geo_source
WHERE country IN (SELECT code FROM EuropeUpdate);


-- Geo_estuary
PROMPT Passen die Tabelle Geo_estuary an 
INSERT INTO geo_estuary(river, country, province)
SELECT DISTINCT g.river, 'EU', e.name
FROM geo_estuary g, EuropeUpdate e
WHERE g.country IN (SELECT code FROM EuropeUpdate)
AND g.country = e.code
AND g.province != 'Sachsen' AND g.province != 'Bayern';


UPDATE geo_estuary
SET country = 'EU'
WHERE province = 'Sachsen' OR province = 'Bayern';


DELETE FROM geo_estuary
WHERE country IN (SELECT code FROM EuropeUpdate);


-- Geo_sea
PROMPT Passen die Tabelle Geo_sea an 
INSERT INTO geo_sea(sea, country, province)
SELECT DISTINCT g.sea, 'EU', e.name
FROM geo_sea g, EuropeUpdate e
WHERE g.country IN (SELECT code FROM EuropeUpdate)
AND g.country = e.code
AND g.province != 'Sachsen' AND g.province != 'Bayern';


UPDATE geo_sea
SET country = 'EU'
WHERE province = 'Sachsen' OR province = 'Bayern';


DELETE FROM geo_sea
WHERE country IN (SELECT code FROM EuropeUpdate);


-- Geo_island
PROMPT Passen die Tabelle Geo_island an 
INSERT INTO geo_island(island, country, province)
SELECT DISTINCT g.island, 'EU', e.name
FROM geo_island g, EuropeUpdate e
WHERE g.country IN (SELECT code FROM EuropeUpdate)
AND g.country = e.code
AND g.province != 'Sachsen' AND g.province != 'Bayern';


UPDATE geo_island
SET country = 'EU'
WHERE province = 'Sachsen' OR province = 'Bayern';


DELETE FROM geo_island
WHERE country IN (SELECT code FROM EuropeUpdate);


-- Geo_mountain
PROMPT Passen die Tabelle Geo_mountain an 
INSERT INTO geo_mountain(mountain, country, province)
SELECT DISTINCT g.mountain, 'EU', e.name
FROM geo_mountain g, EuropeUpdate e
WHERE g.country IN (SELECT code FROM EuropeUpdate)
AND g.country = e.code
AND g.province != 'Sachsen' AND g.province != 'Bayern';


UPDATE geo_mountain
SET country = 'EU'
WHERE province = 'Sachsen' OR province = 'Bayern';


DELETE FROM geo_mountain
WHERE country IN (SELECT code FROM EuropeUpdate);


-- Geo_desert 
PROMPT Passen die Tabelle Geo_desert an 
INSERT INTO geo_desert(desert, country, province)
SELECT DISTINCT g.desert, 'EU', e.name
FROM geo_desert g, EuropeUpdate e
WHERE g.country IN (SELECT code FROM EuropeUpdate)
AND g.country = e.code
AND g.province != 'Sachsen' AND g.province != 'Bayern';


UPDATE geo_desert
SET country = 'EU'
WHERE province = 'Sachsen' OR province = 'Bayern';


DELETE FROM geo_desert
WHERE country IN (SELECT code FROM EuropeUpdate);


-- Located
PROMPT Passen die Tabelle Located an 
UPDATE located
SET country = 'EU', province = (SELECT name FROM EuropeUpdate e WHERE located.country = e.code)
WHERE country IN (SELECT code FROM EuropeUpdate)
AND province != 'Sachsen' AND province != 'Bayern';


UPDATE located
SET country = 'EU'
WHERE province = 'Sachsen' OR province = 'Bayern';


-- LocatedOn
PROMPT Passen die Tabelle LocatedOn an 
UPDATE locatedOn
SET country = 'EU', province = (SELECT name FROM EuropeUpdate e WHERE locatedOn.country = e.code)
WHERE country IN (SELECT code FROM EuropeUpdate)
AND province != 'Sachsen' AND province != 'Bayern';


UPDATE locatedOn
SET country = 'EU'
WHERE province = 'Sachsen' OR province = 'Bayern';


-- Borders
PROMPT Passen die Tabelle Borders an 
INSERT INTO borders (country1, country2, length)
SELECT 'EU', sb.country2, SUM(length)
FROM sb
WHERE sb.country1 IN (SELECT code FROM EuropeUpdate)
AND sb.country2 NOT IN (SELECT code FROM EuropeUpdate)
GROUP BY sb.country2;


DELETE FROM borders 
WHERE country1 IN (SELECT code FROM EuropeUpdate)
OR country2 IN (SELECT code FROM EuropeUpdate);


-- Politics
PROMPT Passen die Tabelle Politics an 
INSERT INTO politics(country, dependent, independence, wasdependent, government)
VALUES ('EU', NULL, sysdate, NULL, (SELECT government FROM politics WHERE country = 'D'));


DELETE FROM politics WHERE country IN (SELECT code FROM EuropeUpdate);


-- Economy
PROMPT Passen die Tabelle Economy an 
INSERT INTO economy(country, GDP, agriculture, service, industry, inflation, unemployment)
VALUES ('EU', 
		(SELECT SUM(GDP) FROM economy WHERE country IN (SELECT code FROM EuropeUpdate)),
		(SELECT SUM(GDP * agriculture) / SUM(GDP) FROM economy WHERE country IN (SELECT code FROM EuropeUpdate)),
		(SELECT SUM(GDP * service) / SUM(GDP) FROM economy WHERE country IN (SELECT code FROM EuropeUpdate)),
		(SELECT SUM(GDP * industry) / SUM(GDP) FROM economy WHERE country IN (SELECT code FROM EuropeUpdate)),
		(SELECT SUM(GDP * inflation) / SUM(GDP) FROM economy WHERE country IN (SELECT code FROM EuropeUpdate)),
		(SELECT (SUM(population * unemployment))/ SUM(population) 
			FROM economy, EuropeUpdate 
			WHERE country IN (SELECT code FROM EuropeUpdate) 
			AND economy.country = EuropeUpdate.code)
	   );


DELETE FROM economy WHERE country IN (SELECT code FROM EuropeUpdate);

-- Population
PROMPT Passen die Tabelle Population an 
INSERT INTO population(country, population_growth, infant_mortality)
VALUES ('EU', 
	   (SELECT SUM(e.population * p.population_growth) / SUM(e.population) FROM EuropeUpdate e, population p WHERE e.code = p.country),
	   (SELECT SUM(e.population * p.infant_mortality) / SUM(e.population) FROM EuropeUpdate e, population p WHERE e.code = p.country)
	   );


DELETE FROM population WHERE country IN (SELECT code FROM EuropeUpdate);


-- Encompasses
PROMPT Passen die Tabelle Encompasses an 
INSERT INTO encompasses (continent, country, percentage)
VALUES ('Europe', 'EU', (SELECT SUM(area) FROM EuropeUpdate)*100 / (SELECT area FROM country WHERE code ='EU'));


DELETE FROM encompasses WHERE country IN (SELECT code FROM EuropeUpdate);


-- Language
PROMPT Passen die Tabelle Language an 
INSERT INTO language(country, name, percentage)
SELECT 'EU', name, SUM(pop)*100 / (SELECT population FROM country WHERE code='EU')
FROM (SELECT l.name, l.country, l.percentage * e.population/100 AS pop 
	  FROM language l, europeupdate e 
	  WHERE l.country = e.code)
GROUP BY name;


DELETE FROM language WHERE country IN (SELECT code FROM EuropeUpdate);


-- Religion
PROMPT Passen die Tabelle Religion an 
INSERT INTO religion(country, name, percentage)
SELECT 'EU', name, SUM(pop)*100 / (SELECT population FROM country WHERE code='EU')
FROM (SELECT r.name, r.country, r.percentage * e.population/100 AS pop 
	  FROM religion r, europeupdate e 
	  WHERE r.country = e.code)
GROUP BY name;


DELETE FROM religion WHERE country IN (SELECT code FROM EuropeUpdate);


-- EthnicGroup
PROMPT Passen die Tabelle EthnicGroup an 
INSERT INTO ethnicgroup(country, name, percentage)
SELECT 'EU', name, SUM(pop)*100 / (SELECT population FROM country WHERE code='EU')
FROM (SELECT eg.name, eg.country, eg.percentage * e.population/100 AS pop 
	  FROM ethnicgroup eg, europeupdate e 
	  WHERE eg.country = e.code)
GROUP BY name;


DELETE FROM ethnicgroup WHERE country IN (SELECT code FROM EuropeUpdate);

PROMPT Löschen die Tabelle EuropeUpdate

DROP TABLE EuropeUpdate;

