CREATE OR REPLACE VIEW cstatsc AS
 SELECT code, government, gdp, popGrowth, infMort, popDens, continent
 FROM cstats, encompasses
 WHERE code=country
  AND percentage>=50;

SELECT * FROM user_updatable_columns
WHERE Table_Name='CSTATSC'; --alle Werte au�er Continent aus cstats abgeleitet

UPDATE cstatsc
SET continent='Africa'
WHERE code='R';
--SELECT * FROM encompasses WHERE country='R';
