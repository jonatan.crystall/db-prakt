ALTER SESSION SET NLS_DATE_FORMAT = 'DD MM SYYYY';
-- SYYYY means 4-digit-year, S prefixes BC years with "-"

CREATE TABLE Population
(Country VARCHAR2(4) CONSTRAINT PopKey PRIMARY KEY,
 Population_Growth NUMBER,
 Infant_Mortality NUMBER);

CREATE TABLE Language
(Country VARCHAR2(4),
Name VARCHAR2(50),
Percentage NUMBER CONSTRAINT LanguagePercent
CHECK ((Percentage > 0) AND (Percentage <= 100)),
CONSTRAINT LanguageKey PRIMARY KEY (Name, Country));

CREATE TABLE Religion
(Country VARCHAR2(4),
Name VARCHAR2(50),
Percentage NUMBER CONSTRAINT ReligionPercent
 CHECK ((Percentage > 0) AND (Percentage <= 100)),
CONSTRAINT ReligionKey PRIMARY KEY (Name, Country));

CREATE TABLE EthnicGroup
(Country VARCHAR2(4),
Name VARCHAR2(50),
Percentage NUMBER CONSTRAINT EthnicPercent
  CHECK ((Percentage > 0) AND (Percentage <= 100)),
CONSTRAINT EthnicKey PRIMARY KEY (Name, Country));

CREATE TABLE encompasses
(Country VARCHAR2(4) NOT NULL,
 Continent VARCHAR2(20) NOT NULL,
 Percentage NUMBER,
   CHECK ((Percentage > 0) AND (Percentage <= 100)),
 CONSTRAINT EncompassesKey PRIMARY KEY (Country,Continent));

CREATE OR REPLACE TYPE GeoCoord AS OBJECT
(Latitude NUMBER,
Longitude NUMBER);
/

CREATE TABLE geo_Mountain
(Mountain VARCHAR2(50) ,
Country VARCHAR2(4) ,
Province VARCHAR2(50) ,
CONSTRAINT GMountainKey PRIMARY KEY (Province,Country,Mountain) );

CREATE TABLE geo_Desert
(Desert VARCHAR2(50) ,
Country VARCHAR2(4) ,
Province VARCHAR2(50) ,
CONSTRAINT GDesertKey PRIMARY KEY (Province, Country, Desert) );

CREATE TABLE geo_Island
(Island VARCHAR2(50) ,
Country VARCHAR2(4) ,
Province VARCHAR2(50) ,
CONSTRAINT GIslandKey PRIMARY KEY (Province, Country, Island) );

CREATE TABLE geo_River
(River VARCHAR2(50) ,
Country VARCHAR2(4) ,
Province VARCHAR2(50) ,
CONSTRAINT GRiverKey PRIMARY KEY (Province ,Country, River) );

CREATE TABLE geo_Sea
(Sea VARCHAR2(50) ,
Country VARCHAR2(4)  ,
Province VARCHAR2(50) ,
CONSTRAINT GSeaKey PRIMARY KEY (Province, Country, Sea) );

CREATE TABLE geo_Lake
(Lake VARCHAR2(50) ,
Country VARCHAR2(4) ,
Province VARCHAR2(50) ,
CONSTRAINT GLakeKey PRIMARY KEY (Province, Country, Lake) );

CREATE TABLE geo_Source
(River VARCHAR2(50) ,
Country VARCHAR2(4) ,
Province VARCHAR2(50) ,
CONSTRAINT GSourceKey PRIMARY KEY (Province, Country, River) );

CREATE TABLE geo_Estuary
(River VARCHAR2(50) ,
Country VARCHAR2(4) ,
Province VARCHAR2(50) ,
CONSTRAINT GEstuaryKey PRIMARY KEY (Province, Country, River) );

CREATE TABLE located
(City VARCHAR2(50) ,
Province VARCHAR2(50) ,
Country VARCHAR2(4) ,
River VARCHAR2(50),
Lake VARCHAR2(50),
Sea VARCHAR2(50) );

CREATE TABLE locatedOn
(City VARCHAR2(50) ,
 Province VARCHAR2(50) ,
 Country VARCHAR2(4) ,
 Island VARCHAR2(50) ,
 CONSTRAINT locatedOnKey PRIMARY KEY (City, Province, Country, Island) );

CREATE TABLE Airport
(IATACode VARCHAR(3) PRIMARY KEY,
 Name VARCHAR2(100) ,
 Country VARCHAR2(4) ,
 City VARCHAR2(50) ,
 Province VARCHAR2(50) ,
 Island VARCHAR2(50) ,
 Latitude NUMBER CONSTRAINT AirpLat
   CHECK ((Latitude >= -90) AND (Latitude <= 90)) ,
 Longitude NUMBER CONSTRAINT AirpLon
   CHECK ((Longitude >= -180) AND (Longitude <= 180)) ,
 Elevation NUMBER ,
 gmtOffset NUMBER );
