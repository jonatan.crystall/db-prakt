-- User1
CREATE SYNONYM Country FOR dominickleppich.Country;
CREATE SYNONYM City FOR dominickleppich.City;
CREATE SYNONYM Province FOR dominickleppich.Province;
CREATE SYNONYM Economy FOR dominickleppich.Economy;
CREATE SYNONYM Politics FOR dominickleppich.Politics;
CREATE SYNONYM borders FOR dominickleppich.borders;
CREATE SYNONYM Organization FOR dominickleppich.Organization;
CREATE SYNONYM isMember FOR dominickleppich.isMember;


-- User2 (simulated)
CREATE SYNONYM Continent FOR andrejliebert.Continent;
CREATE SYNONYM Mountain FOR andrejliebert.Mountain;
CREATE SYNONYM Desert FOR andrejliebert.Desert;
CREATE SYNONYM Island FOR andrejliebert.Island;
CREATE SYNONYM Lake FOR andrejliebert.Lake;
CREATE SYNONYM Sea FOR andrejliebert.Sea;
CREATE SYNONYM River FOR andrejliebert.River;
CREATE SYNONYM RiverThrough FOR andrejliebert.RiverThrough;
CREATE SYNONYM mergesWith FOR andrejliebert.mergesWith;
CREATE SYNONYM islandIn FOR andrejliebert.islandIn;
CREATE SYNONYM MountainOnIsland FOR andrejliebert.MountainOnIsland;
