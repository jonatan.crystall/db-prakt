-- Population-Country
ALTER TABLE Population
ADD(CONSTRAINT PopCountry FOREIGN KEY (Country) REFERENCES Country(Code) ON DELETE CASCADE);

-- Language-Country
ALTER TABLE Language
ADD(CONSTRAINT LanCountry FOREIGN KEY (Country) REFERENCES Country(Code) ON DELETE CASCADE);

-- Religion-Country
ALTER TABLE Religion
ADD(CONSTRAINT RelCountry FOREIGN KEY (Country) REFERENCES Country(Code) ON DELETE CASCADE);

-- Ethnicgroup-Country
ALTER TABLE Ethnicgroup
ADD(CONSTRAINT EthCountry FOREIGN KEY (Country) REFERENCES Country(Code) ON DELETE CASCADE);

-- Encompasses-Continent
ALTER TABLE Encompasses
ADD(CONSTRAINT EncompContinent FOREIGN KEY (Continent) REFERENCES Continent(Name) ON DELETE CASCADE);

-- Encompasses-Country
ALTER TABLE Encompasses
ADD(CONSTRAINT EncompCountry FOREIGN KEY (Country) REFERENCES Country(Code) ON DELETE CASCADE);

-- Geo_Mountain-Province
ALTER TABLE Geo_Mountain
ADD(CONSTRAINT GeoMProvince FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_Mountain-Mountain
ALTER TABLE Geo_Mountain
ADD(CONSTRAINT GeoMoMountain FOREIGN KEY (Mountain) REFERENCES Mountain(Name) ON DELETE CASCADE);

-- Geo_Desert-Province
ALTER TABLE Geo_Desert
ADD(CONSTRAINT GeoDesertProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_Desert-Desert
ALTER TABLE Geo_Desert
ADD(CONSTRAINT GeoDeDesert FOREIGN KEY (Desert) REFERENCES Desert(Name) ON DELETE CASCADE);

-- Geo_Island-Province
ALTER TABLE Geo_Island
ADD(CONSTRAINT GeoIslandProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_Island-Island
ALTER TABLE Geo_Island
ADD(CONSTRAINT GeoIslIsland FOREIGN KEY (Island) REFERENCES Island(Name) ON DELETE CASCADE);

-- Geo_River-Province
ALTER TABLE Geo_River
ADD(CONSTRAINT GeoRiverProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_River-River
ALTER TABLE Geo_River
ADD(CONSTRAINT GeoRiverRiver FOREIGN KEY (River) REFERENCES River(Name) ON DELETE CASCADE);

-- Geo_Sea-Province
ALTER TABLE Geo_Sea
ADD(CONSTRAINT GeoSeaProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_Sea-Sea
ALTER TABLE Geo_Sea
ADD(CONSTRAINT GeoSeaSea FOREIGN KEY (Sea) REFERENCES Sea(Name) ON DELETE CASCADE);

-- Geo_Lake-Province
ALTER TABLE Geo_Lake
ADD(CONSTRAINT GeoLakeProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_Lake-Lake
ALTER TABLE Geo_Lake
ADD(CONSTRAINT GeolakeLake FOREIGN KEY (Lake) REFERENCES Lake(Name) ON DELETE CASCADE);

-- Geo_Source-Province
ALTER TABLE Geo_Source
ADD(CONSTRAINT GeoSourceProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_Source-River
ALTER TABLE Geo_Source
ADD(CONSTRAINT GeoSourceRiver FOREIGN KEY (River) REFERENCES River(Name) ON DELETE CASCADE);

-- Geo_Estuary-Province
ALTER TABLE Geo_Estuary
ADD(CONSTRAINT GeoEstuaryProv FOREIGN KEY (Country, Province) REFERENCES Province(Country, Name) ON DELETE CASCADE);

-- Geo_Estuary-River
ALTER TABLE Geo_Estuary
ADD(CONSTRAINT GeoEstuaryRiver FOREIGN KEY (River) REFERENCES River(Name) ON DELETE CASCADE);

-- Located-City
ALTER TABLE Located
ADD(CONSTRAINT LocCity FOREIGN KEY (City, Country, Province) REFERENCES City(Name, Country, Province) ON DELETE CASCADE);

-- Located-River
ALTER TABLE Located
ADD(CONSTRAINT LocRiver FOREIGN KEY (River) REFERENCES River(Name) ON DELETE SET NULL);

-- Located-Lake
ALTER TABLE Located
ADD(CONSTRAINT LocLake FOREIGN KEY (Lake) REFERENCES Lake(Name) ON DELETE SET NULL);

-- Located-Sea
ALTER TABLE Located
ADD(CONSTRAINT LocSea FOREIGN KEY (Sea) REFERENCES Sea(Name) ON DELETE SET NULL);

-- LocatedOn-City
ALTER TABLE LocatedOn
ADD(CONSTRAINT LocOnCity FOREIGN KEY (City, Country, Province) REFERENCES City(Name, Country, Province) ON DELETE CASCADE);

-- LocatedOn-Island
ALTER TABLE LocatedOn
ADD(CONSTRAINT LocOnIsland FOREIGN KEY (Island) REFERENCES Island(Name) ON DELETE CASCADE);
