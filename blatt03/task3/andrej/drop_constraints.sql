ALTER TABLE Lake
DROP CONSTRAINT LakeRiver;

ALTER TABLE River
DROP CONSTRAINT RiverRiver;

ALTER TABLE River
DROP CONSTRAINT RiverLake;

ALTER TABLE River
DROP CONSTRAINT RiverSea;

ALTER TABLE RiverThrough
DROP CONSTRAINT RiverthRiver;

ALTER TABLE RiverThrough
DROP CONSTRAINT RiverthLake;

ALTER TABLE MergesWith
DROP CONSTRAINT MergesWS1;

ALTER TABLE MergesWith
DROP CONSTRAINT MergeswS2;

ALTER TABLE IslandIn
DROP CONSTRAINT IslINIsl;

ALTER TABLE IslandIn
DROP CONSTRAINT IslINSea;

ALTER TABLE IslandIn
DROP CONSTRAINT IslINLake;

ALTER TABLE IslandIn
DROP CONSTRAINT IslINRiv;

ALTER TABLE MountainOnIsland
DROP CONSTRAINT MOIslandMountain;

ALTER TABLE MountainOnIsland
DROP CONSTRAINT MOIslandIsland;
