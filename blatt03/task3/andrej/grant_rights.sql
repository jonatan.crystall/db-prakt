grant select on Continent to jonatancrystall;
grant references on Continent to jonatancrystall;

grant select on Mountain to jonatancrystall;
grant references on Mountain to jonatancrystall;

grant select on Desert to jonatancrystall;
grant references on Desert to jonatancrystall;

grant select on Island to jonatancrystall;
grant references on Island to jonatancrystall;

grant select on Lake to jonatancrystall;
grant references on Lake to jonatancrystall;

grant select on Sea to jonatancrystall;
grant references on Sea to jonatancrystall;

grant select on River to jonatancrystall;
grant references on River to jonatancrystall;

grant select on RiverThrough to jonatancrystall;
grant references on RiverThrough to jonatancrystall;

grant select on mergesWith to jonatancrystall;
grant references on mergesWith to jonatancrystall;

grant select on islandIn to jonatancrystall;
grant references on islandIn to jonatancrystall;

grant select on MountainOnIsland to jonatancrystall;
grant references on MountainOnIsland to jonatancrystall;
