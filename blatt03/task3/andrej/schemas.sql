ALTER SESSION SET NLS_DATE_FORMAT = 'DD MM SYYYY';
-- SYYYY means 4-digit-year, S prefixes BC years with "-"

  CREATE TABLE Continent
  (Name VARCHAR2(20) CONSTRAINT ContinentKey PRIMARY KEY,
   Area NUMBER(10));

   CREATE TABLE Mountain
   (Name VARCHAR2(50) CONSTRAINT MountainKey PRIMARY KEY,
    Mountains VARCHAR2(50),
    Elevation NUMBER,
    Type VARCHAR2(10),
    Coordinates GeoCoord CONSTRAINT MountainCoord
        CHECK ((Coordinates.Latitude >= -90) AND
               (Coordinates.Latitude <= 90) AND
               (Coordinates.Longitude > -180) AND
               (Coordinates.Longitude <= 180)));

   CREATE TABLE Desert
   (Name VARCHAR2(50) CONSTRAINT DesertKey PRIMARY KEY,
    Area NUMBER,
    Coordinates GeoCoord CONSTRAINT DesCoord
        CHECK ((Coordinates.Latitude >= -90) AND
               (Coordinates.Latitude <= 90) AND
               (Coordinates.Longitude > -180) AND
               (Coordinates.Longitude <= 180)));

   CREATE TABLE Island
   (Name VARCHAR2(50) CONSTRAINT IslandKey PRIMARY KEY,
    Islands VARCHAR2(50),
    Area NUMBER CONSTRAINT IslandAr check (Area >= 0),
    Elevation NUMBER,
    Type VARCHAR2(10),
    Coordinates GeoCoord CONSTRAINT IslandCoord
        CHECK ((Coordinates.Latitude >= -90) AND
               (Coordinates.Latitude <= 90) AND
               (Coordinates.Longitude > -180) AND
               (Coordinates.Longitude <= 180)));

   CREATE TABLE Lake
   (Name VARCHAR2(50) CONSTRAINT LakeKey PRIMARY KEY,
    River VARCHAR2(50),
    Area NUMBER CONSTRAINT LakeAr CHECK (Area >= 0),
    Elevation NUMBER,
    Depth NUMBER CONSTRAINT LakeDpth CHECK (Depth >= 0),
    Height NUMBER CONSTRAINT DamHeight CHECK (Height > 0),
    Type VARCHAR2(12),
    Coordinates GeoCoord CONSTRAINT LakeCoord
        CHECK ((Coordinates.Latitude >= -90) AND
               (Coordinates.Latitude <= 90) AND
               (Coordinates.Longitude > -180) AND
               (Coordinates.Longitude <= 180)));

   CREATE TABLE Sea
   (Name VARCHAR2(50) CONSTRAINT SeaKey PRIMARY KEY,
    Area NUMBER CONSTRAINT SeaAr CHECK (Area >= 0),
    Depth NUMBER CONSTRAINT SeaDepth CHECK (Depth >= 0));

   CREATE TABLE River
   (Name VARCHAR2(50) CONSTRAINT RiverKey PRIMARY KEY,
    River VARCHAR2(50),
    Lake VARCHAR2(50),
    Sea VARCHAR2(50),
    Length NUMBER CONSTRAINT RiverLength
      CHECK (Length >= 0),
    Area NUMBER CONSTRAINT RiverArea
      CHECK (Area >= 0),
    Source GeoCoord CONSTRAINT SourceCoord
        CHECK ((Source.Latitude >= -90) AND
               (Source.Latitude <= 90) AND
               (Source.Longitude > -180) AND
               (Source.Longitude <= 180)),
    Mountains VARCHAR2(50),
    SourceElevation NUMBER,
    Estuary GeoCoord CONSTRAINT EstCoord
        CHECK ((Estuary.Latitude >= -90) AND
               (Estuary.Latitude <= 90) AND
               (Estuary.Longitude > -180) AND
               (Estuary.Longitude <= 180)),
    EstuaryElevation NUMBER,
    CONSTRAINT RivFlowsInto
        CHECK ((River IS NULL AND Lake IS NULL)
               OR (River IS NULL AND Sea IS NULL)
               OR (Lake IS NULL AND Sea is NULL)));

CREATE TABLE RiverThrough
(River VARCHAR2(50),
Lake  VARCHAR2(50),
CONSTRAINT RThroughKey PRIMARY KEY (River,Lake) );

CREATE TABLE mergesWith
(Sea1 VARCHAR2(50) ,
 Sea2 VARCHAR2(50) ,
 CONSTRAINT MergesWithKey PRIMARY KEY (Sea1, Sea2) );

CREATE TABLE islandIn
(Island VARCHAR2(50) ,
 Sea VARCHAR2(50) ,
 Lake VARCHAR2(50) ,
 River VARCHAR2(50) );

CREATE TABLE MountainOnIsland
(Mountain VARCHAR2(50),
 Island   VARCHAR2(50),
 CONSTRAINT MountainIslKey PRIMARY KEY (Mountain, Island) );
