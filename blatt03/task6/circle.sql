CREATE TABLE testCountry
(
	Code VARCHAR2(20) PRIMARY KEY,
	Capital VARCHAR2(20),
	Province VARCHAR2(20),
	CapCountry VARCHAR2(20)
);

CREATE TABLE testProvince
(
	Country VARCHAR2(20),
	Name VARCHAR2(20),
	Capital VARCHAR2(20),
	CapProvince VARCHAR2(20),
	CapCountry VARCHAR2(20),
	CONSTRAINT ProvPK PRIMARY KEY (Country, Name)
);

CREATE TABLE testCity
(
	Country VARCHAR2(20),
	Province VARCHAR2(20),
	Name VARCHAR2(20),
	CONSTRAINT CitPK PRIMARY KEY (Country, Province, Name)
);

ALTER TABLE testCountry
	ADD CONSTRAINT CouRefsCity
		FOREIGN KEY (CapCountry, Province, Capital) REFERENCES testCity(Country, Province, Name)
		ON DELETE SET NULL
		DEFERRABLE
	ADD CONSTRAINT CouRefsProvince
		FOREIGN KEY (CapCountry, Province) REFERENCES testProvince(Country, Name)
		ON DELETE CASCADE
		DEFERRABLE;

ALTER TABLE testProvince
	ADD CONSTRAINT ProvRefsCountry
		FOREIGN KEY (Country) REFERENCES testCountry(Code)
		ON DELETE CASCADE
		DEFERRABLE
	ADD CONSTRAINT ProvRefsCity
		FOREIGN KEY (CapCountry, CapProvince, Capital) REFERENCES testCity(Country, Province, Name)
		ON DELETE CASCADE
		DEFERRABLE;

ALTER TABLE testCity
	ADD CONSTRAINT CityRefsCountry
		FOREIGN KEY (Country) REFERENCES testCountry(Code)
		ON DELETE SET NULL
		DEFERRABLE
	ADD CONSTRAINT CityRefsProvince
		FOREIGN KEY (Country, Province) REFERENCES testProvince(Country, Name)
		ON DELETE CASCADE
		DEFERRABLE;

SET CONSTRAINTS ALL DEFERRED;

INSERT INTO testCountry VALUES ('DE', 'GOE', 'NDS', 'DE');
INSERT INTO testCountry VALUES ('GB', 'LON', 'ENG', 'GB');
INSERT INTO testCountry VALUES ('FR', 'BRE', 'BR', 'FR');

INSERT INTO testProvince VALUES ('DE', 'NDS', 'GOE', 'NDS', 'DE');
INSERT INTO testProvince VALUES ('GB', 'ENG', 'LON', 'ENG', 'GB');
INSERT INTO testProvince VALUES ('FR', 'BR', 'BRE', 'BR', 'FR');

INSERT INTO testCity VALUES ('DE', 'NDS', 'GOE');
INSERT INTO testCity VALUES ('GB', 'ENG', 'LON');
INSERT INTO testCity VALUES ('FR', 'BR', 'BRE');

COMMIT;
