# Task 5

**Anmerkung:** Änderungen bei anderen Usern erst nach `COMMIT`

## 1a)
A bekommt eine Fehlermeldung `Child record found`

## 1b)
A kann Tupel löschen, bei Commit werden referenzierende Tupel bei B gelöscht.

## 1c)
A schränkt sich durch `GRANT REFERENCES` ein, dadurch wird allerdings di Konsistenz der Datenbank über alle Benutzer erhalten, da keine dangling references entstehen können.

## 1d)
Entweder: Entsprechende Tupel bei User B löschen oder Holzhammer:
`REVOKE REFERENCES ON T1 FROM <User B>`, dann wird der Constraint bei User B gelöscht.

## 2a)
Wenn B ein Update durchführt, werden die entsprechenden Spalten in T geändert, solange die Spalten in V direkt aus T kommen. Bei abgeleiteten Spalten gibt es eine Fehlermeldung, es kann ja auch nicht bestimmt werden, was geändert werden soll.
Wenn etwas gelöscht wird, wird das entsprechende Tupel auch in T gelöscht.

## 2b)
Wenn Rechte auf ein View vergeben werden, werden damit auch implizit Rechte auf die "Quelltabellen" gegeben. Dies ist aus Konsistenzgründen auch sinnvoll, sonst wäre ein View plötzlich anders als seine Quelle.